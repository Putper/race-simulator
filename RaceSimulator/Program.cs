﻿using System;
using System.Threading;
using Controller;
using Model;

namespace RaceSimulator
{
    class Program
    {
        static void Main(string[] args)
        {
            // start a competition
            Competition competition = new Competition();
            Data.Initialise(competition);
            // select track
            Data.NextRace();
            //Data.NextRace();
            //Data.NextRace();
            //Data.NextRace();
            //Data.NextRace();
            // bind visualisation
            BindEvents();
            // start race
            Data.CurrentRace.Start();

            while (true)
            {
                //Data.CurrentRace.MoveParticipants(DateTime.Now);
                //Data.CurrentRace.BreakParticipants();
                //Visualisation.DrawTrack(Data.CurrentRace.Track);
                Thread.Sleep(500);
            }
        }

        /// <summary>
        /// rebind events when a new race is started
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        static void OnRaceFinished(object sender, RaceFinishedEventArgs args)
        {
            BindEvents();
        }

        /// <summary>
        /// bind visualisation and data events
        /// </summary>
        static void BindEvents()
        {
            if(Data.CurrentRace != null)
            {
                Data.CurrentRace.RaceFinished += Data.OnRaceFinished;
                Data.CurrentRace.DriversChanged += Visualisation.OnDriversChanged;
                Data.CurrentRace.RaceFinished += Visualisation.OnRaceFinished;
                Data.CurrentRace.RaceFinished += OnRaceFinished;
            }
        }
    }
}
