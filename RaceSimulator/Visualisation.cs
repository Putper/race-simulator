﻿using Model;
using Controller;
using System;
using System.Collections.Generic;
using System.Text;
using System.Security;
using Model.RaceData;

namespace RaceSimulator
{
    public static class Visualisation
    {
        /// <summary>
        /// width of each section
        /// </summary>
        const int GRID_SIZE_X = 5;
        /// <summary>
        /// height of each section
        /// </summary>
        const int GRID_SIZE_Y = 4;
        /// <summary>
        /// which direction to start drawing the track in
        /// </summary>
        const Directions START_DIRECTION = Directions.East;

        /// <summary>
        /// which debug line its currently on
        /// </summary>
        private static int _debugY { get; set; }

        #region graphics
        /// <summary>
        /// -----
        /// 1>
        ///   2>
        /// -----
        /// </summary>
        private static string[] _startHorizontal = { "-----", "1>   ", "  2> ", "-----" };
        /// <summary>
        /// |1  |
        /// |V 2|
        /// |  V|
        /// |   |
        /// </summary>
        private static string[] _startVertical = { "|1  |", "|V 2|", "|  V|", "|   |" };
        /// <summary>
        /// ----
        ///  #1
        ///  #2
        /// ----
        /// </summary>
        private static string[] _finishHorizontal = { "-----", "  #1 ", "  #2 ", "-----" };
        /// <summary>
        /// |   |
        /// |# #|
        /// |1 2|
        /// |   |
        /// </summary>
        private static string[] _finishVertical = { "|   |", "|# #|", "|1 2|", "|   |" };
        /// <summary>
        /// -----
        ///   1
        ///   2
        /// -----
        /// </summary>
        private static string[] _straightHorizontal = { "-----", "  1  ", "  2  ", "-----" };
        /// <summary>
        /// |   |
        /// |   |
        /// |1 2|
        /// |   |
        /// </summary>
        private static string[] _straightVertical = { "|   |", "|   |", "|1 2|", "|   |" };
        /// <summary>
        /// ---\
        ///    1\
        ///  2  |
        /// \   |
        /// </summary>
        private static string[] _cornerLeftToBottom = { "---\\ ", "   1\\", " 2  |", "\\   |" };
        /// <summary>
        /// /   |
        ///  1  |
        ///    2/
        /// ---/
        /// </summary>
        private static string[] _cornerLeftToTop = { "/   |", " 1  |",  "   2/", "---/ " };
        /// <summary>
        ///  /---
        /// /2
        /// |  1
        /// |   /
        /// </summary>
        private static string[] _cornerRightToBottom = { " /---", "/2   ", "|  1 ", "|   /" };
        /// <summary>
        /// |   \
        /// |  1
        /// \2
        ///  \---
        /// </summary>
        private static string[] _cornerRightToTop = { "|   \\", "|  1 ", "\\2   ", " \\---" };
        #endregion

        public static void Initialise()
        {
            Console.CursorVisible = false;
        }

        /// <summary>
        /// Draw a given track in the console
        /// </summary>
        /// <param name="track">which track to visualise</param>
        public static void DrawTrack(Track track)
        {
            Console.Clear();

            // position on grid
            int[] startPosition = GetStartPositionForTrack(track, new Compass(START_DIRECTION));
            int xPos = startPosition[0];
            int yPos = startPosition[1];
            Compass compass = new Compass(START_DIRECTION);

            // draw sections
            foreach (Section section in track.Sections)
            {
                SectionData sectionData = Data.CurrentRace.GetSectionData(section);
                IParticipant participant1 = sectionData.Left;
                IParticipant participant2 = sectionData.Right;
                // draw section
                string[] sectionGraphics = GetSectionGraphics(section.SectionType, compass);
                for(int i=0; i < sectionGraphics.Length; ++i)
                {
                    string toDraw = PlaceParticipantsOnPart(sectionGraphics[i], participant1, participant2);
                    Console.SetCursorPosition(xPos * GRID_SIZE_X, yPos * GRID_SIZE_Y+i);
                    Console.Write(toDraw);
                }

                // handle new direction for corners
                if (section.SectionType == SectionTypes.RightCorner)
                {
                    compass.RotateClockwise();
                }
                else if (section.SectionType == SectionTypes.LeftCorner)
                {
                    compass.RotateAntiClockwise();
                }
                // decide new position (move 1 forward in current direction)
                switch (compass.Direction)
                {
                    case Directions.North:
                        yPos -= 1;
                        break;
                    case Directions.East:
                        xPos += 1;
                        break;
                    case Directions.South:
                        yPos += 1;
                        break;
                    default: // Directions.West
                        xPos -= 1;
                        break;
                }
            }

            // debug
            _debugY = 0;
            DebugShowLaps();
            DebugShowPositions();
        }

        /// <summary>
        /// draw a scoreboard, parameters may be null
        /// </summary>
        /// <param name="pointsList"></param>
        /// <param name="brokenList"></param>
        /// <param name="failedToFixList"></param>
        public static void DrawScoreboard(RaceDataList<ParticipantPoints> pointsList, RaceDataList<ParticipantEquipmentBroken> brokenList, RaceDataList<ParticipantFailedToFixEquipment> failedToFixList)
        {
            Console.Clear();
            Console.SetCursorPosition(0, 0);

            Console.WriteLine("SCOREBOARD");
            Console.WriteLine();
            if (pointsList != null)
            {
                string best = pointsList.Best();
                if (best != null)
                {
                    Console.WriteLine("Most Points: " + best);
                }
            }
            if (brokenList != null)
            {
                string best = brokenList.Best();
                if (best != null)
                {
                    Console.WriteLine("Had their vehicle destroyed the least: " + best);
                }
            }
            if (failedToFixList != null)
            {
                string best = failedToFixList.Best();
                if (best != null)
                {
                    Console.WriteLine("Failed to fix their broken vehicle the least: " + best);
                }
            }
        }

        /// <summary>
        /// Get grid position where to start drawing
        /// </summary>
        /// <param name="track"></param>
        /// <param name="compass"></param>
        /// <returns>an int array of size 2, where index 0 is the xpos and index 1 is the ypos</returns>
        private static int[] GetStartPositionForTrack(Track track, Compass compass)
        {
            int xPos = 0;
            int yPos = 0;
            int lowestXPos = 0;
            int lowestYPos = 0;
            // calculating initial x&y pos
            bool first = true;
            foreach (Section section in track.Sections)
            {
                // don't move forward for the first one cuz its always drawn on 0,0
                if (!first)
                {
                    // move one forward
                    switch (compass.Direction)
                    {
                        case Directions.North:
                            yPos -= 1;
                            break;
                        case Directions.East:
                            xPos += 1;
                            break;
                        case Directions.South:
                            yPos += 1;
                            break;
                        default: // Directions.West
                            xPos -= 1;
                            break;
                    }
                }
                first = false;
                // handle new direction for corners
                if (section.SectionType == SectionTypes.RightCorner)
                {
                    compass.RotateClockwise();
                }
                else if (section.SectionType == SectionTypes.LeftCorner)
                {
                    compass.RotateAntiClockwise();
                }
                // store if lower
                if (xPos < lowestXPos)
                {
                    lowestXPos = xPos;
                }
                if (yPos < lowestYPos)
                {
                    lowestYPos = yPos;
                }
            }

            // if the track goes as far as 6 sections to the left from start (lowestXPos=6).
            // That means we want it to start drawing the track 6 positions from the right,
            // so it has space for the -6 part of the track
            // We do this by making the number positive (-6 to 6) and removing 1 (6 to 5)
            // we remove 1 because it starts counting at 0, so 6 sections is 0-5
            if (lowestXPos < 0)
            {
                lowestXPos = lowestXPos * -1;
            }
            if (lowestYPos < 0)
            {
                lowestYPos = lowestYPos * -1;
            }
            return new int[] { lowestXPos, lowestYPos};
        }

        /// <summary>
        /// get the correct graphics array for a given section & direction
        /// </summary>
        /// <param name="section">which section to get the graphics for</param>
        /// <param name="compass">which direction the section is heading to</param>
        /// <returns></returns>
        private static string[] GetSectionGraphics(SectionTypes section, Compass compass)
        {
            switch (section)
            {
                case SectionTypes.Straight:
                    return compass.IsHorizontal() ? _straightHorizontal : _straightVertical;

                case SectionTypes.StartGrid:
                    return compass.IsHorizontal() ? _startHorizontal : _startVertical;

                case SectionTypes.Finish:
                    return compass.IsHorizontal() ? _finishHorizontal : _finishVertical;

                case SectionTypes.RightCorner:
                    switch (compass.Direction)
                    {
                        case Directions.North:
                            return _cornerRightToBottom;
                        case Directions.East:
                            return _cornerLeftToBottom;
                        case Directions.South:
                            return _cornerLeftToTop;
                        default: // Directions.West
                            return _cornerRightToTop;
                    }

                case SectionTypes.LeftCorner:
                    switch (compass.Direction)
                    {
                        case Directions.North:
                            return _cornerLeftToBottom;
                        case Directions.East:
                            return _cornerLeftToTop;
                        case Directions.South:
                            return _cornerRightToTop;
                        default: // Directions.West
                            return _cornerRightToBottom;
                    }

                default:
                    return new string[0];
            }
        }

        /// <summary>
        /// Places given participants on the track.
        /// if null is given for the participants the lane is left empty.
        /// </summary>
        /// <param name="lane">a part of the track</param>
        /// <param name="participant1">Participant on the left side of the track</param>
        /// <param name="participant2">Participant on the right side of the track</param>
        /// <returns></returns>
        private static string PlaceParticipantsOnPart(string lane, IParticipant participant1, IParticipant participant2)
        {
            char p1 = ' ';
            if (participant1 != null)
            {
                p1 = (participant1.Equipment.IsBroken) ? 'X' : participant1.TeamColor.ToString()[0];
            }
            char p2 = ' ';
            if (participant2 != null)
            {
                p2 = (participant2.Equipment.IsBroken) ? 'X' : participant2.TeamColor.ToString()[0];
            }
            lane = lane.Replace('1', p1);
            lane = lane.Replace('2', p2);
            return lane;
        }

        /// <summary>
        /// when drivers are changed, redraw the track
        /// </summary>
        /// <param name="args">used to get the current track</param>
        public static void OnDriversChanged(object sender, DriversChangedEventArgs args)
        {
            DrawTrack(args.Track);
        }

        /// <summary>
        /// when race finishes, go to next race
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public static void OnRaceFinished(object sender, RaceFinishedEventArgs args)
        {
            if(Data.CurrentRace != null)
            {
                DrawTrack(Data.CurrentRace.Track);
            }
            else
            {
                DrawScoreboard(Data.Competition.ParticipantsPoints, Data.Competition.ParticipantsEquipmentBroken, Data.Competition.ParticipantsFailedToFixEquipment);
            }
        }

        /// <summary>
        /// used to write debugging statements, showed seperate from the map
        /// </summary>
        /// <param name="toWrite">what to show</param>
        private static void DebugWrite(string toWrite)
        {
            int x = Console.BufferWidth - toWrite.ToString().Length;
            Console.SetCursorPosition(x, _debugY);
            _debugY++;
            Console.WriteLine(toWrite);
        }

        /// <summary>
        /// visualise laps with text for debugging
        /// </summary>
        private static void DebugShowLaps()
        {
            //foreach (KeyValuePair<IParticipant, int> entry in Data.CurrentRace._laps)
            //{
            //    DebugWrite(entry.Key.Name + "    " + entry.Value);
            //}
        }

        /// <summary>
        /// visualise positions
        /// </summary>
        private static void DebugShowPositions()
        {
            //foreach (KeyValuePair<Section, SectionData> entry in Data.CurrentRace._positions)
            //{
            //    Console.WriteLine(entry.Key.SectionType);
            //    string left = entry.Value.Left == null ? "-" : entry.Value.Left.Name;
            //    string right = entry.Value.Right == null ? "-" : entry.Value.Right.Name;
            //    DebugWrite("\t" + entry.Value.DistanceLeft + "\t" + left);
            //    DebugWrite("\t" + entry.Value.DistanceRight + "\t" + right);
            //}
        }
    }
}
