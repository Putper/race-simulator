﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace View
{
    /// <summary>
    /// Interaction logic for RaceLeaderboardWindow.xaml
    /// </summary>
    public partial class RaceLeaderboardWindow : Window
    {
        public RaceLeaderboardWindow()
        {
            InitializeComponent();
            Data.CurrentRace.RaceFinished += OnRaceFinished;
            BindEvents();
        }

        /// <summary>
        /// rebind events when a new race is started
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void OnRaceFinished(object sender, RaceFinishedEventArgs args)
        {
            BindEvents();
        }

        private void BindEvents()
        {
            this.Dispatcher.Invoke(() =>
            {
                RaceLeaderboardWindowDataContext dataContext = (RaceLeaderboardWindowDataContext)this.DataContext;
                dataContext.CurrentRace = Data.CurrentRace;
                if(Data.CurrentRace != null)
                {
                    Data.CurrentRace.DriversChanged += dataContext.OnDriversChanged;
                    Data.CurrentRace.RaceFinished += OnRaceFinished;
                }
            });
        }
    }
}
