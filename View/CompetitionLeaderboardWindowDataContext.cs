﻿using Model;
using Model.RaceData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace View
{
    public class CompetitionLeaderboardWindowDataContext
    {
        public Competition Competition;

        public ObservableCollection<ParticipantPoints> ParticipantsPoints { get; set; } = new ObservableCollection<ParticipantPoints>();
        public ObservableCollection<ParticipantEquipmentBroken> ParticipantsEquipmentBroken { get; set; } = new ObservableCollection<ParticipantEquipmentBroken>();
        public ObservableCollection<ParticipantFailedToFixEquipment> ParticipantsFailedToFixEquipment { get; set; } = new ObservableCollection<ParticipantFailedToFixEquipment>();

        public void OnRaceFinished(object sender, RaceFinishedEventArgs args)
        {
            App.Current.Dispatcher.Invoke(() =>
            {
                // points
                ParticipantsPoints.Clear();
                foreach (ParticipantPoints pp in Competition.ParticipantsPoints.GetList())
                {
                    ParticipantsPoints.Add(pp);
                }

                // broken equipment
                ParticipantsEquipmentBroken.Clear();
                foreach (ParticipantEquipmentBroken entry in Competition.ParticipantsEquipmentBroken.GetList())
                {
                    ParticipantsEquipmentBroken.Add(entry);
                }

                // failed to fix equipment
                ParticipantsFailedToFixEquipment.Clear();
                foreach (ParticipantFailedToFixEquipment entry in Competition.ParticipantsFailedToFixEquipment.GetList())
                {
                    ParticipantsFailedToFixEquipment.Add(entry);
                }
            });
        }
    }
}
