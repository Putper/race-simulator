﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Media.Imaging;

namespace View
{
    class Visualisation
    {
        private const int SECTION_SIZE = 150;
        private const int PARTICIPANT_SIZE = 45;
        #region GRAPHICS
        private const string START_FILE = ".\\Images\\Roads\\Straight.png";
        private const string FINISH_FILE = ".\\Images\\Roads\\Finish.png";
        private const string STRAIGHT_FILE = ".\\Images\\Roads\\Straight.png";
        private const string CORNER_FILE= ".\\Images\\Roads\\Corner.png";
        private const string PARTICIPANT_FILE = ".\\Images\\Participants\\Sheep.png";
        private const string PARTICIPANT_BROKEN_FILE = ".\\Images\\Participants\\SheepBroken.png";
        private const string GRASS_FILE = ".\\Images\\Backgrounds\\Grass.png";
        #endregion

        /// <summary>
        /// returns a drawn background of given size
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public static BitmapSource DrawBackground(int width, int height)
        {
            Bitmap empty = BitmapCache.EmptyBitmap(width, height);
            Bitmap background = BitmapCache.GetBitmap(GRASS_FILE);
            BitmapCache.FillPattern(Graphics.FromImage(empty), background, new Rectangle(0, 0, width, height));
            return BitmapCache.CreateBitmapSourceFromGdiBitmap(empty);
        }

        /// <summary>
        /// returns a drawn track
        /// </summary>
        /// <param name="track">track to draw</param>
        /// <returns></returns>
        public static BitmapSource DrawTrack(Track track)
        {
            Compass compass = new Compass(Directions.East);
            TrackDimensions dimensions = TrackDimensions.FromTrack(track, compass.Direction, SECTION_SIZE);
            int xPos = dimensions.StartXPos;
            int yPos = dimensions.StartYPos;

            Bitmap bitmap = BitmapCache.EmptyBitmap(dimensions.Width, dimensions.Height);
            Graphics graphics = Graphics.FromImage(bitmap);
            foreach (Section section in track.Sections)
            {
                // draw section
                DrawSection(graphics, section.SectionType, xPos, yPos, compass.Direction);
                // draw participants
                SectionData sectionData = Data.CurrentRace.GetSectionData(section);
                DrawParticipants(graphics, section.SectionType, xPos, yPos, compass.Direction, sectionData.Left, sectionData.Right);

                // handle new direction for corners
                if (section.SectionType == SectionTypes.RightCorner)
                {
                    compass.RotateClockwise();
                }
                else if (section.SectionType == SectionTypes.LeftCorner)
                {
                    compass.RotateAntiClockwise();
                }
                // decide new position (move 1 forward in current direction)
                switch (compass.Direction)
                {
                    case Directions.North:
                        yPos -= SECTION_SIZE;
                        break;
                    case Directions.East:
                        xPos += SECTION_SIZE;
                        break;
                    case Directions.South:
                        yPos += SECTION_SIZE;
                        break;
                    default: // Directions.West
                        xPos -= SECTION_SIZE;
                        break;
                }
            }

            return BitmapCache.CreateBitmapSourceFromGdiBitmap(bitmap);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="section"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="direction"></param>
        private static void DrawSection(Graphics graphics, SectionTypes sectionType, int x, int y, Directions direction)
        {
            Compass compass = new Compass(direction);
            string file;
            int rotation;

            switch (sectionType)
            {
                case SectionTypes.StartGrid:
                    file = START_FILE;
                    rotation = compass.ToInt();
                    break;

                case SectionTypes.Finish:
                    file = FINISH_FILE;
                    rotation = compass.ToInt();
                    break;

                case SectionTypes.RightCorner:
                case SectionTypes.LeftCorner:
                    CornerTypes cornerType = Section.GetCornerType(sectionType, direction);
                    file = CORNER_FILE;
                    switch (cornerType)
                    {
                        case CornerTypes.RightToBottom:
                            rotation = 3;
                            break;
                        case CornerTypes.LeftToBottom:
                            rotation = 0; 
                            break;
                        case CornerTypes.LeftToTop:
                            rotation = 1; 
                            break;
                        default: // right to top
                            rotation = 2; 
                            break;
                    }
                    break;

                default: // SectionTypes.Straight
                    file = STRAIGHT_FILE;
                    rotation = compass.ToInt();
                    break;
            }

            Bitmap bitmap = BitmapCache.GetBitmap(file, rotation);
            graphics.DrawImage(bitmap, x, y, SECTION_SIZE, SECTION_SIZE);
        }

        /// <summary>
        /// draw participants that belong on this track
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="sectionType"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="direction"></param>
        /// <param name="leftParticipant"></param>
        /// <param name="rightParticipant"></param>
        private static void DrawParticipants(Graphics graphics, SectionTypes sectionType, int x, int y, Directions direction, IParticipant leftParticipant, IParticipant rightParticipant)
        {
            Compass compass = new Compass(direction); 
            if (leftParticipant != null)
            {
                (int xOffset, int yOffset) = ParticipantOffset(sectionType, true, compass);
                DrawParticipant(graphics, leftParticipant, x + xOffset, y + yOffset, compass);
            }
            if(rightParticipant != null)
            {
                (int xOffset, int yOffset) = ParticipantOffset(sectionType, false, compass);
                DrawParticipant(graphics, rightParticipant, x + xOffset, y + yOffset, compass);
            }
        }

        /// <summary>
        /// draw given participant at set position
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="participant"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="direction"></param>
        private static void DrawParticipant(Graphics graphics, IParticipant participant, int x, int y, Compass compass)
        {
            string file = (participant.Equipment.IsBroken) ? PARTICIPANT_BROKEN_FILE : PARTICIPANT_FILE;
            Bitmap bitmap = BitmapCache.GetBitmap(file, compass.ToInt());

            Color colour = Color.FromName(participant.TeamColor.ToString());
            graphics.DrawImage(
                bitmap,
                new Rectangle(x, y, PARTICIPANT_SIZE, PARTICIPANT_SIZE), // destination
                0,0, // source start
                PARTICIPANT_SIZE, // source width
                PARTICIPANT_SIZE, // source height
                GraphicsUnit.Pixel,
                GetParticipantReplacementColours(colour)
            );
        }

        /// <summary>
        /// get ImageAttributes for the recolouring of the participant
        /// </summary>
        /// <param name="colour">colour to colour the participant to</param>
        /// <returns></returns>
        private static ImageAttributes GetParticipantReplacementColours(Color colour)
        {

            ImageAttributes imageAttributes = new ImageAttributes();
            ColorMap[] remapTable =
            {
                // replace white
                new ColorMap()
                {
                    OldColor = Color.White,
                    NewColor = colour,
                },
                // replace light gray
                new ColorMap()
                {
                    OldColor = Color.FromArgb(193, 193, 189),
                    NewColor = BitmapCache.ChangeColorBrightness(colour, -0.2f),
                },
                // replace dark gray
                new ColorMap()
                {
                    OldColor = Color.FromArgb(145, 145, 136),
                    NewColor = BitmapCache.ChangeColorBrightness(colour, -0.4f),
                },

            };
            imageAttributes.SetRemapTable(remapTable, ColorAdjustType.Bitmap);

            return imageAttributes;
        }

        /// <summary>
        /// get offset for participant on the given section
        /// </summary>
        /// <param name="sectionType"></param>
        /// <param name="left"></param>
        /// <param name="compass"></param>
        /// <returns></returns>
        private static (int xOffset, int yOffset) ParticipantOffset(SectionTypes sectionType, bool left, Compass compass)
        {
            int xOffset = 0;
            int yOffset = 0;
            int rotation = compass.ToInt();
            switch(sectionType)
            {
                case SectionTypes.StartGrid:
                    yOffset = SECTION_SIZE / 100 * 47;
                    xOffset = (left) ? sectionPercentage(18) : sectionPercentage(52);
                    break;

                case SectionTypes.Finish:
                    yOffset = 0;
                    xOffset = (left) ? sectionPercentage(24) : sectionPercentage(53);
                    break;

                case SectionTypes.RightCorner:
                case SectionTypes.LeftCorner:
                    xOffset = (left) ? sectionPercentage(13) : sectionPercentage(27);
                    yOffset = (left) ? sectionPercentage(53) : sectionPercentage(27);
                    CornerTypes cornerType = Section.GetCornerType(sectionType, compass.Direction);
                    switch (cornerType)
                    {
                        case CornerTypes.RightToBottom:
                            rotation = 3;
                            break;
                        case CornerTypes.LeftToBottom:
                            rotation = 0;
                            break;
                        case CornerTypes.LeftToTop:
                            rotation = 1;
                            break;
                        default: // right to top
                            rotation = 2;
                            break;
                    }
                    break;

                default: // SectionTypes.Straight
                    yOffset = sectionPercentage(47);
                    xOffset = (left) ? sectionPercentage(24) : sectionPercentage(53);
                    break;
            }

            // rotate offset
            int notRotatedXOffset = xOffset;
            int notRotatedYOffset = yOffset;
            switch (rotation)
            {
                case 1:
                    xOffset = SECTION_SIZE - notRotatedYOffset - PARTICIPANT_SIZE;
                    yOffset = notRotatedXOffset;
                    break;
                case 2:
                    xOffset = SECTION_SIZE - notRotatedXOffset - PARTICIPANT_SIZE;
                    yOffset = SECTION_SIZE - notRotatedYOffset - PARTICIPANT_SIZE;
                    break;
                case 3:
                    xOffset = notRotatedYOffset;
                    yOffset = SECTION_SIZE - notRotatedXOffset - PARTICIPANT_SIZE;
                    break;
                default:
                    break;
            }

            return (xOffset, yOffset);
        }

        /// <summary>
        /// return percentage of a section
        /// </summary>
        /// <param name="percentage"></param>
        /// <returns></returns>
        private static int sectionPercentage(int percentage)
        {
            return (int)(SECTION_SIZE / 100f * percentage);
        }

        /// <summary>
        /// when drivers are changed, redraw the track
        /// </summary>
        /// <param name="args">used to get the current track</param>
        public static void OnDriversChanged(object sender, DriversChangedEventArgs args)
        {
            //DrawTrack(args.Track);
        }


        /// <summary>
        /// when race finishes, go to next race
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public static void OnRaceFinished(object sender, RaceFinishedEventArgs args)
        {
            //if (Data.CurrentRace != null)
            //{
                //DrawTrack(Data.CurrentRace.Track);
            //}
            //else
            //{
            //    DrawScoreboard(Data.Competition.ParticipantsPoints, Data.Competition.ParticipantsEquipmentBroken, Data.Competition.ParticipantsFailedToFixEquipment);
            //}
        }
    }
}
