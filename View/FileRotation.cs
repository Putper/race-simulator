﻿using System;
using System.Collections.Generic;
using System.Text;

namespace View
{
    class FileRotation : IEquatable<FileRotation>
    {
        public string File { get; set; }

        public int Rotation { get; set; }

        public FileRotation(string file, int rotation)
        {
            File = file;
            Rotation = rotation;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as FileRotation);
        }

        public bool Equals(FileRotation other)
        {
            return other != null &&
                   File == other.File &&
                   Rotation == other.Rotation;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(File, Rotation);
        }
    }
}
