﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace View
{
    public class MainWindowDataContext : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _trackName = "No current track";
        public string TrackName
        {
            get
            {
                return _trackName;
            }
            set
            {
                _trackName = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TrackName"));
            }
        }

        public MainWindowDataContext()
        {

        }

        /// <summary>
        /// called when a driver is changed in the race
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void OnDriversChanged(object sender, DriversChangedEventArgs args)
        {
            TrackName = args.Track.Name;
        }
    }
}
