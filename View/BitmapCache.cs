﻿using Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Media.Imaging;

namespace View
{
    static class BitmapCache
    {
        private const string EMPTY_BITMAP_KEY = "empty";
        private static Dictionary<string, Bitmap> _bitmaps = new Dictionary<string, Bitmap>();

        /// <summary>
        /// get a bitmap from given filename
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static Bitmap GetBitmap(string file)
        {
            return GetBitmap(file, 0);
        }

        /// <summary>
        /// get a bitmap from given filename and apply given rotation
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="rotation">rotation 0=0, 1=90degrees, 2=180 degrees, 3=270degrees</param>
        /// <returns></returns>
        public static Bitmap GetBitmap(string filename, int rotation)
        {
            // get bitmap
            if (!_bitmaps.ContainsKey(filename))
            {
                _bitmaps.Add(filename, new Bitmap(filename));
            }
            Bitmap bitmap = _bitmaps[filename].Clone() as Bitmap;

            // apply rotation
            RotateFlipType rotateFlipType;
            switch (rotation)
            {
                case 1:
                    rotateFlipType = RotateFlipType.Rotate90FlipNone;
                    break;
                case 2:
                    rotateFlipType = RotateFlipType.Rotate180FlipNone;
                    break;
                case 3:
                    rotateFlipType = RotateFlipType.Rotate270FlipNone;
                    break;
                default:
                    rotateFlipType = RotateFlipType.RotateNoneFlipNone;
                    break;
            }
            bitmap.RotateFlip(rotateFlipType);
            return bitmap;
        }

        /// <summary>
        /// get an empty bitmap with a given size
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public static Bitmap EmptyBitmap(int width, int height)
        {
            string key = $"{EMPTY_BITMAP_KEY}_{width}_{height}";
            if(!_bitmaps.ContainsKey(key))
            {
                Bitmap empty = new Bitmap(width, height);
                _bitmaps.Add(key, empty);
                Graphics graphics = Graphics.FromImage(empty);
                //SolidBrush brush = new SolidBrush(Color.ForestGreen);
                //graphics.FillRectangle(brush, 0, 0, width, height);
            }
            return _bitmaps[key].Clone() as Bitmap;
        }

        /// <summary>
        /// SOURCE: https://stackoverflow.com/a/2675314
        /// </summary>
        /// <param name="g"></param>
        /// <param name="image"></param>
        /// <param name="rect"></param>
        public static void FillPattern(Graphics g, Image image, Rectangle rect)
        {
            Rectangle imageRect;
            Rectangle drawRect;

            for (int x = rect.X; x < rect.Right; x += image.Width)
            {
                for (int y = rect.Y; y < rect.Bottom; y += image.Height)
                {
                    drawRect = new Rectangle(x, y, Math.Min(image.Width, rect.Right - x),
                                   Math.Min(image.Height, rect.Bottom - y));
                    imageRect = new Rectangle(0, 0, drawRect.Width, drawRect.Height);

                    g.DrawImage(image, drawRect, imageRect, GraphicsUnit.Pixel);
                }
            }
        }

        /// <summary>
        /// clear bitmap cache.
        /// call when you're sure you wont use any of the images anymore
        /// </summary>
        public static void Clear()
        {
            _bitmaps.Clear();
        }

        /// <summary>
        /// convert a bitmap to a bitmapsource
        /// </summary>
        /// <param name="bitmap">bitmap to convert</param>
        /// <returns></returns>
        public static BitmapSource CreateBitmapSourceFromGdiBitmap(Bitmap bitmap)
        {
            if (bitmap == null)
                throw new ArgumentNullException("bitmap");

            var rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);

            var bitmapData = bitmap.LockBits(
                rect,
                ImageLockMode.ReadWrite,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            try
            {
                var size = (rect.Width * rect.Height) * 4;

                return BitmapSource.Create(
                    bitmap.Width,
                    bitmap.Height,
                    bitmap.HorizontalResolution,
                    bitmap.VerticalResolution,
                    System.Windows.Media.PixelFormats.Bgra32,
                    null,
                    bitmapData.Scan0,
                    size,
                    bitmapData.Stride);
            }
            finally
            {
                bitmap.UnlockBits(bitmapData);
            }
        }

        /// <summary>
        /// darken or lighten a given colour
        /// SOURCE: https://gist.github.com/zihotki/09fc41d52981fb6f93a81ebf20b35cd5
        /// </summary>
        /// <param name="color"></param>
        /// <param name="correctionFactor"></param>
        /// <returns></returns>
        public static Color ChangeColorBrightness(Color color, float correctionFactor)
        {
            float red = (float)color.R;
            float green = (float)color.G;
            float blue = (float)color.B;

            if (correctionFactor < 0)
            {
                correctionFactor = 1 + correctionFactor;
                red *= correctionFactor;
                green *= correctionFactor;
                blue *= correctionFactor;
            }
            else
            {
                red = (255 - red) * correctionFactor + red;
                green = (255 - green) * correctionFactor + green;
                blue = (255 - blue) * correctionFactor + blue;
            }

            return Color.FromArgb(color.A, (int)red, (int)green, (int)blue);
        }
    }
}
