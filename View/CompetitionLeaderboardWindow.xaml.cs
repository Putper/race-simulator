﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace View
{
    /// <summary>
    /// Interaction logic for CompetitionLeaderboardWindow.xaml
    /// </summary>
    public partial class CompetitionLeaderboardWindow : Window
    {
        public CompetitionLeaderboardWindow()
        {
            InitializeComponent();
            Data.CurrentRace.RaceFinished += OnRaceFinished;
            BindEvents();
        }

        /// <summary>
        /// rebind events when a new race is started
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void OnRaceFinished(object sender, RaceFinishedEventArgs args)
        {
            BindEvents();
        }

        private void BindEvents()
        {
            this.Dispatcher.Invoke(() =>
            {
                CompetitionLeaderboardWindowDataContext dataContext = (CompetitionLeaderboardWindowDataContext)this.DataContext;
                dataContext.Competition = Data.Competition;
                if (Data.CurrentRace != null)
                {
                    Data.CurrentRace.RaceFinished += dataContext.OnRaceFinished;
                    Data.CurrentRace.RaceFinished += OnRaceFinished;
                }
            });
        }
    }
}
