﻿using Controller;
using Model;
using Model.RaceData;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace View
{
    class RaceLeaderboardWindowDataContext
    {
        private Race _currentRace { get; set; }
        public Race CurrentRace
        {
            get
            {
                return _currentRace;
            }
            set
            {
                _currentRace = value;
                // reset all properties
                _amountFinished = 0;
                FinishedParticipants.Clear();
                BrokenEquipments.Clear();
                FailedToFixEquipment.Clear();
            }
        }

        /// <summary>
        /// keeps track of finished participants
        /// </summary>
        public ObservableCollection<IParticipant> FinishedParticipants { get; set; } = new ObservableCollection<IParticipant>();
        /// <summary>
        /// keeps track of how often each driver had their equipment broken
        /// </summary>
        public ObservableCollection<ParticipantEquipmentBroken> BrokenEquipments { get; set; } = new ObservableCollection<ParticipantEquipmentBroken>();
        /// <summary>
        /// keeps track of how often each driver failed to fix their equipment
        /// </summary>
        public ObservableCollection<ParticipantFailedToFixEquipment> FailedToFixEquipment { get; set; } = new ObservableCollection<ParticipantFailedToFixEquipment>();
        private int _amountFinished = 0; // keeps track of how many have finished

        public RaceLeaderboardWindowDataContext()
        {
        }

        /// <summary>
        /// called when a driver is changed in the race
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void OnDriversChanged(object sender, DriversChangedEventArgs args)
        {
            // reset local finishedparticipants property if a new participant finished
            // a queue is turned into an array here x,x
            App.Current.Dispatcher.Invoke(() =>
            {
                // finished participants
                if (CurrentRace.FinishedParticipants.Count > _amountFinished)
                {
                    FinishedParticipants.Clear();
                    foreach (IParticipant participant in CurrentRace.FinishedParticipants.ToArray())
                    {
                        FinishedParticipants.Add(participant);
                    }
                }

                // broken equipment
                BrokenEquipments.Clear();
                foreach (KeyValuePair<IParticipant, int> entry in CurrentRace.ParticipantsTimesBroken)
                {
                    BrokenEquipments.Add(new ParticipantEquipmentBroken(entry.Key.Name, entry.Value));
                }

                // failed to fix equipment
                FailedToFixEquipment.Clear();
                foreach (KeyValuePair<IParticipant, int> entry in CurrentRace.ParticipantsTimesFailedToFix)
                {
                    FailedToFixEquipment.Add(new ParticipantFailedToFixEquipment(entry.Key.Name, entry.Value));
                }
            });
        }
    }
}
