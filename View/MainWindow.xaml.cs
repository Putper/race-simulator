﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private CompetitionLeaderboardWindow _competitionLeaderboardWindow;
        private RaceLeaderboardWindow _raceLeaderboardWindow;

        public MainWindow()
        {
            Data.Initialise(new Competition());
            Data.NextRace();
            BindEvents();
            InitializeComponent();
            MainGrid.SizeChanged += MainGrid_SizeChanged;
            Data.CurrentRace.Start();
        }

        private void MainGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if(e.NewSize.Width != 0 && e.NewSize.Height != 0)
            {
                MainGrid.Background = new ImageBrush()
                {
                    ImageSource = Visualisation.DrawBackground((int)MainGrid.RenderSize.Width, (int)MainGrid.RenderSize.Height)
                };
            }
        }

        private void OnDriversChanged(object sender, DriversChangedEventArgs e)
        {
            TrackImage.Dispatcher.BeginInvoke(
                DispatcherPriority.Render,
                new Action(() =>
                {
                    TrackImage.Source = Visualisation.DrawTrack(e.Track);
                })
            );

            // prevent tthread errors
            // source: https://stackoverflow.com/a/9732853
            this.Dispatcher.Invoke(() =>
            {
                ((MainWindowDataContext)this.DataContext).OnDriversChanged(sender, e);
            });
        }


        /// <summary>
        /// rebind events when a new race is started
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void OnRaceFinished(object sender, RaceFinishedEventArgs args)
        {
            if(Data.CurrentRace != null)
            {
                BindEvents();
            }
            else
            {
                this.Dispatcher.Invoke(() =>
                {
                    TrackImage.Height = 0;
                    TrackImage.Width = 0;
                    WinnerPrefixLabel.Content = "Winner: ";
                    WinnerLabel.Content = Data.Competition.ParticipantsPoints.Best();
                });
            }
        }

        /// <summary>
        /// bind visualisation and data events
        /// </summary>
        private void BindEvents()
        {
            if (Data.CurrentRace != null)
            {
                // clear cache first
                Data.CurrentRace.RaceFinished += delegate (object sender, RaceFinishedEventArgs args)
                {
                    BitmapCache.Clear();
                };
                Data.CurrentRace.RaceFinished += Data.OnRaceFinished;
                Data.CurrentRace.DriversChanged += OnDriversChanged;
                //Data.CurrentRace.DriversChanged += ((MainWindowDataContext)this.DataContext).OnDriversChanged;
                Data.CurrentRace.DriversChanged += Visualisation.OnDriversChanged;
                Data.CurrentRace.RaceFinished += Visualisation.OnRaceFinished;
                Data.CurrentRace.RaceFinished += OnRaceFinished;
            }
        }

        /// <summary>
        /// menu item to exit application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void MenuItem_CompetitionLeaderboard_Click(object sender, RoutedEventArgs e)
        {
            _competitionLeaderboardWindow = new CompetitionLeaderboardWindow();
            _competitionLeaderboardWindow.Show();
        }

        private void MenuItem_RaceLeaderboard_Click(object sender, RoutedEventArgs e)
        {
            _raceLeaderboardWindow = new RaceLeaderboardWindow();
            _raceLeaderboardWindow.Show();
        }
    }
}
