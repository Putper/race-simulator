﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;

namespace Controller
{
    public static class Data
    {
        public static Competition Competition { get; set; }
        public static Race CurrentRace { get; set; }

        /// <summary>
        /// initialise competition
        /// </summary>
        /// <param name="competition"></param>
        public static void Initialise(Competition competition)
        {
            Competition = competition;
            InitialiseParticipants();
            InitialiseTracks();
        }

        /// <summary>
        /// create and add participants to the competition
        /// </summary>
        private static void InitialiseParticipants()
        {
            // create a driver for each team colour
            // in future participants could be random
            // or created with instructions from the user
            // but for now this'll work :)
            foreach (TeamColours colour in Enum.GetValues(typeof(TeamColours)))
            {
                string name = colour.ToString() + "Driver";
                Driver participant = new Driver(name, colour);
                Competition.Participants.Add(participant);
            }
        }

        /// <summary>
        /// define all tracks and add them to the competition
        /// </summary>
        private static void InitialiseTracks()
        {
            #region tracks
            Competition.Tracks.Enqueue(new Track("You Spin Me Right Round", new SectionTypes[] {
                SectionTypes.StartGrid,
                SectionTypes.Finish,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.LeftCorner,
             }));

            Competition.Tracks.Enqueue(new Track("Square", new SectionTypes[] {
                SectionTypes.StartGrid,
                SectionTypes.Finish,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
             }));

            Competition.Tracks.Enqueue(new Track("Elburg", new SectionTypes[] {
                SectionTypes.StartGrid,
                SectionTypes.Finish,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.RightCorner,
                SectionTypes.RightCorner,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
            }));

            Competition.Tracks.Enqueue(new Track("Gun", new SectionTypes[] {
                SectionTypes.StartGrid,
                SectionTypes.Finish,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.RightCorner,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
            }));

            Competition.Tracks.Enqueue(new Track("Hammer", new SectionTypes[] {
                SectionTypes.StartGrid,
                SectionTypes.Finish,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.RightCorner,
                SectionTypes.LeftCorner,
                SectionTypes.LeftCorner,
                SectionTypes.RightCorner,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.LeftCorner,
            }));
            #endregion tracks
        }

        /// <summary>
        /// start the next track
        /// </summary>
        public static void NextRace()
        {
            Track nextTrack = Competition.NextTrack();
            if (nextTrack != null)
            {
                CurrentRace = new Race(nextTrack, Competition.Participants, 500);
                CurrentRace.InitialiseParticipants();
            }
            else
            {
                CurrentRace = null;
            }
        }

        /// <summary>
        /// when race finishes, go to next race
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public static void OnRaceFinished(object sender, RaceFinishedEventArgs args)
        {
            Competition.AddParticipantPoints(CurrentRace.FinishedParticipants);
            Competition.AddParticipantsEquipmentBroken(CurrentRace.ParticipantsTimesBroken);
            Competition.AddParticipantFailedToFixEquipment(CurrentRace.ParticipantsTimesFailedToFix);
            NextRace();
            if(CurrentRace != null)
            {
                CurrentRace.Start();
            }
        }
    }
}
