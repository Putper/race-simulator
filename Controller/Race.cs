﻿using Model;
using Model.RaceData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace Controller
{
    public class Race
    {
        /// <summary>
        /// length if a section in metres
        /// </summary>
        public const int SECTION_LENGTH = 100;

        /// <summary>
        /// Current Track
        /// </summary>
        public Track Track { get; set; }

        /// <summary>
        /// Drivers on the track
        /// </summary>
        public List<IParticipant> Participants { get; set; }

        /// <summary>
        /// Event that's called when the drivers have moved
        /// </summary>
        public event EventHandler<DriversChangedEventArgs> DriversChanged;

        /// <summary>
        /// Event that's called when all drivers passed the finish
        /// </summary>
        public event EventHandler<RaceFinishedEventArgs> RaceFinished;

        /// <summary>
        /// a stack of all participants that finished.
        /// whoever finishes first is enqueued first.
        /// </summary>
        public Queue<IParticipant> FinishedParticipants { get; set; } = new Queue<IParticipant>();

        /// <summary>
        /// keeps track how often each participants had their equipment broken
        /// </summary>
        public Dictionary<IParticipant, int> ParticipantsTimesBroken = new Dictionary<IParticipant, int>();

        /// <summary>
        /// keeps track how often each participant failed to fix their broken equipment
        /// </summary>
        public Dictionary<IParticipant, int> ParticipantsTimesFailedToFix = new Dictionary<IParticipant, int>();

        /// <summary>
        /// DataList of the time it took for each participant to pass a section
        /// </summary>
        public RaceDataList<SectionTime> SectionTimes = new RaceDataList<SectionTime>();

        /// <summary>
        /// a dictionary which keeps track of when a participant last passed a section.
        /// is used for the SectionTimes DataList.
        /// </summary>
        private Dictionary<IParticipant, DateTime> _participantLastPassedSection = new Dictionary<IParticipant, DateTime>();

        /// <summary>
        /// How many laps each participant has performed.
        /// </summary>
        private Dictionary<IParticipant, int> _laps { get; set; }

        /// <summary>
        /// Each section has its own data
        /// used to know who's on the track
        /// </summary>
        private Dictionary<Section, SectionData> _positions;

        /// <summary>
        /// how many laps until a racer is finished
        /// </summary>
        private int _maxLaps { get; set; }

        /// <summary>
        /// when the race started
        /// </summary>
        private DateTime _startTime { get; set; }

        private Random _random { get; set; }

        private Timer _timer { get; set; }


        /// <param name="track">track to race on</param>
        /// <param name="participants">participants who will be joining this race</param>
        /// <param name="timerTime">time between moving participants</param>
        public Race(Track track, List<IParticipant> participants, int timerTime) : this(track, participants, 2, timerTime)
        { }

        /// <param name="track">track to race on</param>
        /// <param name="participants">participants who will be joining this race</param>
        /// <param name="laps">amount of laps before winner is decided</param>
        /// <param name="timerTime">time between moving participants</param>
        public Race(Track track, List<IParticipant> participants, int laps, int timerTime)
        {
            Track = track;
            Participants = participants;
            _maxLaps = laps;
            _laps = new Dictionary<IParticipant, int>();
            _positions = new Dictionary<Section, SectionData>();
            _random = new Random(DateTime.Now.Millisecond);
            _timer = new Timer(timerTime);
            _timer.Elapsed += OnTimedEvent;
        }

        /// <summary>
        /// initialise
        /// </summary>
        public void InitialiseParticipants()
        {
            RandomiseEquipment();
            _startTime = DateTime.Now;

            // register initial values for data dictonaries
            _participantLastPassedSection.Clear();
            ParticipantsTimesBroken.Clear();
            ParticipantsTimesFailedToFix.Clear();
            foreach (IParticipant participant in Participants)
            {
                _participantLastPassedSection.Add(participant, _startTime);
                ParticipantsTimesBroken.Add(participant, 0);
                ParticipantsTimesFailedToFix.Add(participant, 0);
                if (_laps.ContainsKey(participant))
                {
                    _laps[participant] = -1;
                } else
                {
                    _laps.Add(participant, -1);
                }
            }

            PlaceParticipants();
        }

        /// <summary>
        /// start the race
        /// </summary>
        public void Start()
        {
            // start timer
            _timer.Start();
        }

        /// <summary>
        /// Get the sectiondata of a given section from this race
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        public SectionData GetSectionData(Section section)
        {
            // create new sectionData if there isn't one available
            if (!_positions.ContainsKey(section))
            {
                SectionData sectionData = new SectionData();
                _positions.Add(section, sectionData);
                return sectionData;
            }
            return _positions[section];
        }

        /// <summary>
        /// Randomise the stats of each participant
        /// </summary>
        public void RandomiseEquipment()
        {
            foreach (IParticipant participant in Participants)
            {
                participant.Equipment.IsBroken = false;
                participant.Equipment.Performance = _random.Next(1, 3);
                participant.Equipment.Speed = _random.Next(18, 33);
            }
        }

        /// <summary>
        /// Place all participants on the track
        /// </summary>
        public void PlaceParticipants()
        {
            LinkedListNode<Section> currentSection = Track.GetStartGrid();
            SectionData currentData = GetSectionData(currentSection.Value);

            // for each participant
            // try placing it on the left side of the section, else the right side
            // if this section is full, go to the previous section
            foreach (IParticipant participant in Participants)
            {
                if (currentData.Left == null)
                {
                    currentData.Left = participant;
                }
                else if (currentData.Right == null)
                {
                    currentData.Right = participant;
                }
                else
                {
                    currentSection = currentSection.Previous;
                    if (currentSection == null)
                    {
                        currentSection = Track.Sections.Last;
                    }
                    currentData = GetSectionData(currentSection.Value);
                    currentData.Left = participant;
                }
            }
        }

        /// <summary>
        /// When the timer passes, move all drivers
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            MoveParticipants(e.SignalTime);
            BreakParticipants();
        }

        /// <summary>
        /// Moves the participants on the sectiondata
        /// based on the participants' equipment
        /// </summary>
        /// OPTIMISE: only call drivers changed after the method instead for each movement
        public void MoveParticipants(DateTime dateTime)
        {
            // go through each section
            // we have to go through each track and then move the
            // participants from the end of the list to the beginning
            LinkedListNode<Section> currentSection = Track.Sections.First;
            while (currentSection != null)
            {
                // there's only participants on this track if there's data for it
                if (_positions.ContainsKey(currentSection.Value))
                {
                    SectionData data = GetSectionData(currentSection.Value);
                    // move car on left side (if there is one)
                    if (data.Left != null)
                    {
                        // dont move if car is broken
                        if (!data.Left.Equipment.IsBroken)
                        {
                            // move person if he hasnt already met the requirements to go to the next
                            if (data.DistanceLeft < SECTION_LENGTH)
                            {
                                int distance = data.Left.Equipment.GetMovement();
                                data.DistanceLeft += distance;
                            }
                            // try to move to next track if he passed this one
                            if (data.DistanceLeft >= SECTION_LENGTH)
                            {
                                int distanceLeftOver = data.DistanceLeft - SECTION_LENGTH;
                                if (PlaceParticipantAt(data.Left, GetNextSection(currentSection), distanceLeftOver, dateTime))
                                {
                                    data.Left = null;
                                    data.DistanceLeft = 0;
                                    DriversChanged?.Invoke(this, new DriversChangedEventArgs(Track));
                                }
                            }
                        }
                    }
                    // move car on right side (if there is one)
                    if (data.Right != null)
                    {
                        // dont move if car is broken
                        if (!data.Right.Equipment.IsBroken)
                        {
                            // move person if he hasnt already met the requirements to go to the next
                            if (data.DistanceRight < SECTION_LENGTH)
                            {
                                int distance = data.Right.Equipment.GetMovement();
                                data.DistanceRight += distance;
                            }
                            // try to move to next track if he passed this one
                            if (data.DistanceRight >= SECTION_LENGTH)
                            {
                                int distanceLeftOver = data.DistanceRight - SECTION_LENGTH;
                                if (PlaceParticipantAt(data.Right, GetNextSection(currentSection), distanceLeftOver, dateTime))
                                {
                                    data.Right = null;
                                    data.DistanceRight = 0;
                                    DriversChanged?.Invoke(this, new DriversChangedEventArgs(Track));
                                }
                            }
                        }
                    }
                }

                currentSection = currentSection.Next;
            }
        }

        /// <summary>
        /// small chance for each participant
        /// to have their equipment destroyed.
        /// If their equipment is already destroyed,
        /// a chance to have it fixed again.
        /// </summary>
        public void BreakParticipants()
        {
            bool changed = false; // if a participant's data has been changed
            foreach (IParticipant participant in Participants)
            {
                // finished participants wont break anymore
                if(FinishedParticipants.Contains(participant))
                {
                    continue;
                }

                // fix
                if (participant.Equipment.IsBroken)
                {
                    if (_random.NextDouble() < 0.1)
                    {
                        changed = true;
                        participant.Equipment.IsBroken = false;
                    }
                    else
                    {
                        ParticipantsTimesFailedToFix[participant] += 1;
                    }
                }
                // break
                else
                {
                    if (_random.NextDouble() < 0.01)
                    {
                        changed = true;
                        participant.Equipment.IsBroken = true;
                        ParticipantsTimesBroken[participant] += 1;
                    }
                }
            }
            // only call driverschanged if a participant actually broke
            if (changed)
                DriversChanged?.Invoke(this, new DriversChangedEventArgs(Track));
        }

        /// <summary>
        /// Moves a participant to the given section
        /// </summary>
        /// <param name="participant">participant to move</param>
        /// <param name="section">which section to place the participant on</param>
        /// <param name="distance">start distance for this track</param>
        /// <returns>
        /// If the participant was successfully moved to the next section.
        /// Returns false if next section is full.
        /// </returns>
        private bool PlaceParticipantAt(IParticipant participant, Section section, int distance, DateTime dateTime)
        {
            SectionData nextData = GetSectionData(section);
            // move to left if theres space
            if (nextData.Left == null)
            {
                nextData.Left = participant;
                nextData.DistanceLeft = distance;
            }
            // move to right if theres space
            else if (nextData.Right == null)
            {
                nextData.Right = participant;
                nextData.DistanceRight = distance;
            }
            // if theres no space, driver can't move to next
            else
            {
                return false;
            }

            // mark time it took to pass section
            RegisterSectionTime(participant, section, dateTime);

            // mark driver's new lap if he passed the finish
            if (section.SectionType == SectionTypes.Finish)
            {
                NextLap(participant, section);
            }

            return true;
        }

        /// <summary>
        /// register when a participant passed a section
        /// </summary>
        /// <param name="participant">the participant that passed the section</param>
        /// <param name="section">the section the participant passed</param>
        /// <param name="dateTime">when the participant passed the section</param>
        private void RegisterSectionTime(IParticipant participant, Section section, DateTime dateTime)
        {
            // the difference is the time he passed this section
            // minus the time he passed the previous section
            TimeSpan difference = dateTime.Subtract(_participantLastPassedSection[participant]);
            SectionTimes.Add(new SectionTime(participant.Name, difference, section));
        }

        /// <summary>
        /// mark a participant for next lap
        /// </summary>
        /// <param name="participant"></param>
        private void NextLap(IParticipant participant, Section currentSection)
        {
            _laps[participant] += 1;
            // remove participant from track if he finished the race
            if (_laps[participant] >= _maxLaps)
            {
                // add to finished list
                FinishedParticipants.Enqueue(participant);
                // remove from track
                SectionData data = GetSectionData(currentSection);
                if (data.Left == participant)
                {
                    data.Left = null;
                }
                else if (data.Right == participant)
                {
                    data.Right = null;
                }

                if(IsRaceFinished())
                {
                    EndRace();
                }
            }
        }

        /// <summary>
        /// Checks if all participants have finished all their laps
        /// </summary>
        /// <returns>If the race is finished</returns>
        private bool IsRaceFinished()
        {
            return FinishedParticipants.Count == Participants.Count;
            //foreach (KeyValuePair<IParticipant, int> entry in _laps)
            //{
            //    // if we found a participant that hasnt finished yet
            //    // we know the race is still going
            //    if (entry.Value < _maxLaps)
            //    {
            //        return false;
            //    }
            //}
            //return true;
        }

        private void EndRace()
        {
            _timer.Stop();
            RaceFinished?.Invoke(this, new RaceFinishedEventArgs());
            EmptyEvents();
        }

        /// <summary>
        /// returns the next section
        /// returns the first if the last is reached
        /// </summary>
        /// <param name="section">section to get the next for</param>
        /// <returns>the next section</returns>
        private Section GetNextSection(LinkedListNode<Section> section)
        {
            return (section.Next != null) ? section.Next.Value : Track.Sections.First.Value;
        }

        /// <summary>
        /// remove all bound events
        /// </summary>
        /// <source>https://stackoverflow.com/a/5475424</source>
        public void EmptyEvents()
        {
            // DriversChanged
            if(DriversChanged != null)
            {
                foreach (EventHandler<DriversChangedEventArgs> d in DriversChanged.GetInvocationList())
                {
                    DriversChanged -= d;
                }
            }
            if (RaceFinished != null)
            {
                // RaceFinished
                foreach (EventHandler<RaceFinishedEventArgs> d in RaceFinished.GetInvocationList())
                {
                    RaceFinished -= d;
                }
            }
        }
    }
}
