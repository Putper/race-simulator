﻿using Controller;
using Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Test
{
    [TestFixture]
    class Controller_Race_DriversChangedShould
    {
        private Track _track;
        private List<IParticipant> _participants;

        [SetUp]
        public void Setup()
        {
            IEquipment car = new Car
            {
                Speed = Race.SECTION_LENGTH,
                Performance = 2
            };
            _participants = new List<IParticipant>() {
                new Driver("Spy", TeamColours.Red) { Equipment = car },
                new Driver("Soldier", TeamColours.Blue) { Equipment = car },
                new Driver("Olivia Mann", TeamColours.Green) { Equipment = car },
                new Driver("Saxton Hale", TeamColours.Yellow) { Equipment = car },
            };

            // a simple circle track
            _track = new Track("You Spin Me Right Round", new SectionTypes[] {
                SectionTypes.StartGrid,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.LeftCorner,
                SectionTypes.Finish
             });
        }

        [Test]
        public void DriversChanged_Called()
        {
            Race race = new Race(_track, _participants, 1);
            race.InitialiseParticipants();

            bool isRan = false;
            race.DriversChanged += (object sender, DriversChangedEventArgs args) =>
            {
                isRan = true;
            };

            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);

            Assert.IsTrue(isRan);
        }

        /// <summary>
        /// participants cant be changed without any participants
        /// </summary>
        [Test]
        public void DriversChanged_NoParticipants_NotCalled()
        {
            Race race = new Race(_track, new List<IParticipant>(), 1);
            race.InitialiseParticipants();

            bool isRan = false;
            race.DriversChanged += delegate (object sender, DriversChangedEventArgs args)
            {
                isRan = true;
            };

            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);

            Assert.IsFalse(isRan);
        }

        /// <summary>
        /// if the race isn't started the drivers can't move
        /// </summary>
        [Test]
        public void DriversChanged_NotStarted_NotCalled()
        {
            Race race = new Race(_track, _participants, 1);
            race.InitialiseParticipants();

            bool isRan = false;
            race.DriversChanged += delegate (object sender, DriversChangedEventArgs args)
            {
                isRan = true;
            };

            Assert.IsFalse(isRan);
        }

        /// <summary>
        /// if events are cleaned, it should never run
        /// </summary>
        public void DriversChanged_EventsEmptied_NotCalled()
        {
            Race race = new Race(_track, _participants, 1);
            race.InitialiseParticipants();

            bool isRan = false;
            race.DriversChanged += (object sender, DriversChangedEventArgs args) =>
            {
                isRan = true;
            };
            race.EmptyEvents();
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);
            race.MoveParticipants(DateTime.Now);

            Assert.IsFalse(isRan);
        }
    }
}
