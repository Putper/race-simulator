﻿using Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_Compass_ToIntShould
    {
        [Test]
        public void ToInt_North_ShouldBe0()
        {
            Compass compass = new Compass(Directions.North);
            int directionInt = compass.ToInt();
            Assert.AreEqual(directionInt, 0);
        }

        [Test]
        public void ToInt_East_ShouldBe1()
        {
            Compass compass = new Compass(Directions.East);
            int directionInt = compass.ToInt();
            Assert.AreEqual(directionInt, 1);
        }

        [Test]
        public void ToInt_South_ShouldBe2()
        {
            Compass compass = new Compass(Directions.South);
            int directionInt = compass.ToInt();
            Assert.AreEqual(directionInt, 2);
        }

        [Test]
        public void ToInt_West_ShouldBe3()
        {
            Compass compass = new Compass(Directions.West);
            int directionInt = compass.ToInt();
            Assert.AreEqual(directionInt, 3);
        }
    }
}
