﻿using Model.RaceData;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_ParticipantFailedToFixEquipment_AddShould
    {
        [Test]
        public void Add_DoesNotContain_ShouldBeAdded()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantFailedToFixEquipment p = new ParticipantFailedToFixEquipment("Henk", 5);
            p.Add(list);

            Assert.Contains(p, list);
        }

        [Test]
        public void Add_AlreadyContains_ShouldntBeAdded()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantFailedToFixEquipment p1 = new ParticipantFailedToFixEquipment("Henk", 5);
            p1.Add(list);
            ParticipantFailedToFixEquipment p2 = new ParticipantFailedToFixEquipment("Henk", 5);
            p2.Add(list);

            Assert.Contains(p1, list);
            CollectionAssert.DoesNotContain(list, p2);
        }

        [Test]
        public void Add_AlreadyContains_FailedToFixShouldBeAdded()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantFailedToFixEquipment p1 = new ParticipantFailedToFixEquipment("Henk", 5);
            p1.Add(list);
            ParticipantFailedToFixEquipment p2 = new ParticipantFailedToFixEquipment("Henk", 5);
            p2.Add(list);

            Assert.AreEqual(p1.TimesFailedToFix, 10);
        }
    }
}
