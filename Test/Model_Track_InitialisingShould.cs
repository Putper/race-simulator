﻿using Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_Track_InitialisingShould
    {

        [Test]
        public void NewTrack_EmptySections_IsEmpty()
        {
            Track track = new Track("Name", new SectionTypes[0]);
            Assert.IsEmpty(track.Sections);
        }

        [Test]
        public void NewTrack_WithSections_Contains()
        {
            SectionTypes[] sectionTypes = new SectionTypes[] { SectionTypes.StartGrid, SectionTypes.Straight, SectionTypes.Finish };
            Track track = new Track("Name", sectionTypes);
            Assert.IsNotEmpty(track.Sections);
            Assert.AreEqual(sectionTypes[0], track.Sections.First.Value.SectionType);
        }
    }
}
