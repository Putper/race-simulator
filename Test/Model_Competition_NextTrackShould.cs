﻿using Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_Competition_NextTrackShould
    {
        private Competition _competition;

        [SetUp]
        public void SetUp()
        {
            _competition = new Competition();
        }

        [Test]
        public void NextTrack_EmptyQueue_ReturnNull()
        {
            Track result = _competition.NextTrack();
            Assert.IsNull(result);
        }

        [Test]
        public void NextTrack_OneInQueue_ReturnTrack()
        {
            Track track = new Track("TrackName", new SectionTypes[0]);
            _competition.Tracks.Enqueue(track);
            Track result = _competition.NextTrack();
            Assert.AreEqual(track, result);
        }

        [Test]
        public void NextTrack_OneInQueue_RemoveTrackFromQueue()
        {
            Track track = new Track("TrackName", new SectionTypes[0]);
            _competition.Tracks.Enqueue(track);
            Track result = _competition.NextTrack();
            result = _competition.NextTrack();
            Assert.IsNull(result);
        }

        [Test]
        public void NextTrack_TwoInQueue_ReturnNextTrack()
        {
            Track track1 = new Track("Track1", new SectionTypes[0]);
            Track track2 = new Track("Track2", new SectionTypes[0]);
            _competition.Tracks.Enqueue(track1);
            _competition.Tracks.Enqueue(track2);
            Track result = _competition.NextTrack();
            Assert.AreEqual(track1, result);
            result = _competition.NextTrack();
            Assert.AreEqual(track2, result);
        }

        [Test]
        public void NextTrack_NotSameTrack()
        {
            Track track1 = new Track("Track1", new SectionTypes[0]);
            Track track2 = new Track("Track2", new SectionTypes[0]);
            _competition.Tracks.Enqueue(track1);
            _competition.Tracks.Enqueue(track2);
            Track track = _competition.NextTrack();
            Assert.AreNotEqual(track, _competition.NextTrack());
        }
    }
}
