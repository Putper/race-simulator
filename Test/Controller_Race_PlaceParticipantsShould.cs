﻿using Controller;
using Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Controller_Race_PlaceParticipantsShould
    {
        private IParticipant _participant1;
        private IParticipant _participant2;
        private IParticipant _participant3;
        private IParticipant _participant4;
        private List<IParticipant> _participants;
        private Track _track;

        [SetUp]
        public void Setup()
        {
            _participant1 = new Driver("Spy", TeamColours.Red);
            _participant2 = new Driver("Soldier", TeamColours.Blue);
            _participant3 = new Driver("Olivia Mann", TeamColours.Green);
            _participant4 = new Driver("Saxton Hale", TeamColours.Yellow);
            _participants = new List<IParticipant>() {
                _participant1, _participant2, _participant3, _participant4
            };

            // a simple circle track
            _track = new Track("You Spin Me Right Round", new SectionTypes[] {
                SectionTypes.StartGrid,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.LeftCorner,
                SectionTypes.Finish
             });
        }

        /// <summary>
        /// ensure that if the method is not ran
        /// that there are no participants on the track
        /// </summary>
        [Test]
        public void PlaceParticipants_NotRan_NotOnTrack()
        {
            Race race = new Race(_track, _participants, 1);

            // ensure track is empty
            LinkedListNode<Section> section = race.Track.Sections.First;
            while (section != null)
            {
                SectionData data = race.GetSectionData(section.Value);
                Assert.IsNull(data.Left);
                Assert.IsNull(data.Right);
                section = section.Next;
            }
        }

        /// <summary>
        /// ensure that if the method is not ran
        /// that there are no participants on the track
        /// </summary>
        [Test]
        public void PlaceParticipants_NoParticipants_IsEmptyTrack()
        {
            Race race = new Race(_track, new List<IParticipant>(), 1);
            race.PlaceParticipants();

            // ensure track is empty
            LinkedListNode<Section> section = race.Track.Sections.First;
            while (section != null)
            {
                SectionData data = race.GetSectionData(section.Value);
                Assert.IsNull(data.Left);
                Assert.IsNull(data.Right);
                section = section.Next;
            }
        }

        /// <summary>
        /// ensure the participants are placed somewhere
        /// and no extra participants are placed
        /// </summary>
        [Test]
        public void PlaceParticipants_ArePlaced()
        {
            Race race = new Race(_track, _participants, 1);
            race.PlaceParticipants();

            // keeps track if participants are placed
            Dictionary<IParticipant, bool> placed = new Dictionary<IParticipant, bool>
            {
                { _participant1, false },
                { _participant2, false },
                { _participant3, false },
                { _participant4, false }
            };

            // find participants in sections
            LinkedListNode<Section> section = race.Track.Sections.First;
            while (section != null)
            {
                SectionData data = race.GetSectionData(section.Value);
                if (data.Left != null)
                {
                    // ensure only participants we defined were placed
                    Assert.IsTrue(placed.ContainsKey(data.Left));
                    placed[data.Left] = true;
                }
                if (data.Right != null)
                {
                    // ensure only participants we defined were placed
                    Assert.IsTrue(placed.ContainsKey(data.Right));
                    placed[data.Right] = true;
                }
                section = section.Next;
            }

            // check if they're all placed
            foreach(KeyValuePair<IParticipant, bool> entry in placed)
            {
                // ensure they're all placed
                Assert.IsTrue(entry.Value);
            }
        }

        /// <summary>
        /// when participants are placed
        /// there should be participants on the track
        /// leftover participants should be placed behind, not in front
        /// </summary>
        [Test]
        public void PlaceParticipants_CorrectlyPlaced()
        {
            Race race = new Race(_track, _participants, 1);
            race.PlaceParticipants();

            LinkedListNode<Section> startSection = race.Track.GetStartGrid();
            // ensure 2 are placed at the start
            SectionData startData = race.GetSectionData(startSection.Value);
            Assert.IsNotNull(startData.Left);
            Assert.IsNotNull(startData.Right);

            // ensure other 2 are placed behind
            LinkedListNode<Section> beforeSection = startSection.Previous;
            if(beforeSection == null)
            {
                beforeSection = race.Track.Sections.Last;
            }
            SectionData beforeData = race.GetSectionData(beforeSection.Value);
            Assert.IsNotNull(beforeData.Left);
            Assert.IsNotNull(beforeData.Right);

            // ensure no-one is placed in front
            LinkedListNode<Section> afterSection = startSection.Next;
            if (afterSection == null)
            {
                afterSection = race.Track.Sections.First;
            }
            SectionData afterData = race.GetSectionData(afterSection.Value);
            Assert.IsNull(afterData.Left);
            Assert.IsNull(afterData.Right);
        }
    }
}
