﻿using Model.RaceData;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_ParticipantFailedToFixEquipment_BestShould
    {
        [Test]
        public void Best_Empty_ShouldReturnNull()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantFailedToFixEquipment p1 = new ParticipantFailedToFixEquipment("Henk", 1);

            string best = p1.Best(list);
            Assert.AreEqual(null, best);
        }

        [Test]
        public void Best_ShouldReturnNameWithLowestFails()
        {
            string winnerName = "Henk";

            List<IRaceData> list = new List<IRaceData>();
            ParticipantFailedToFixEquipment p1 = new ParticipantFailedToFixEquipment("Nick", 10);
            p1.Add(list);
            ParticipantFailedToFixEquipment p2 = new ParticipantFailedToFixEquipment(winnerName, 1);
            p2.Add(list);
            ParticipantFailedToFixEquipment p3 = new ParticipantFailedToFixEquipment("Jasper", 5);
            p3.Add(list);

            string best = p1.Best(list);
            Assert.AreEqual(winnerName, best);
        }
    }
}
