﻿using Model.RaceData;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_ParticipantTime_AddShould
    {
        [Test]
        public void Add_DoesNotContain_ShouldBeAdded()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantTime pt = new ParticipantTime("Henk", TimeSpan.FromSeconds(5));
            pt.Add(list);

            Assert.Contains(pt, list);
        }

        [Test]
        public void Add_AlreadyContains_ShouldntBeAdded()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantTime pt1 = new ParticipantTime("Henk", TimeSpan.FromSeconds(5));
            pt1.Add(list);
            ParticipantTime pt2 = new ParticipantTime("Henk", TimeSpan.FromSeconds(10));
            pt2.Add(list);

            Assert.Contains(pt1, list);
            CollectionAssert.DoesNotContain(list, pt2);
        }

        [Test]
        public void Add_AlreadyContains_TimeIsReplaced()
        {
            TimeSpan newTime = TimeSpan.FromSeconds(10);

            List<IRaceData> list = new List<IRaceData>();
            ParticipantTime pt1 = new ParticipantTime("Henk", TimeSpan.FromSeconds(5));
            pt1.Add(list);
            ParticipantTime pt2 = new ParticipantTime("Henk", newTime);
            pt2.Add(list);

            Assert.AreEqual(newTime, pt1.Time);
        }
    }
}
