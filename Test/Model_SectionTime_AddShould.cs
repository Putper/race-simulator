﻿using Model;
using Model.RaceData;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_SectionTime_AddShould
    {
        private Section _section { get; set; }
        private Section _section2 { get; set; }
        private Section _section3 { get; set; }

        [SetUp]
        public void SetUp()
        {
            _section = new Section() { SectionType = SectionTypes.StartGrid };
            _section2 = new Section() { SectionType = SectionTypes.Straight };
            _section3 = new Section() { SectionType = SectionTypes.Finish };
        }

        [Test]
        public void Add_DoesNotContain_ShouldBeAdded()
        {
            List<IRaceData> list = new List<IRaceData>();
            SectionTime st = new SectionTime("Henk", TimeSpan.FromSeconds(5), _section);
            st.Add(list);

            Assert.Contains(st, list);
        }

        [Test]
        public void Add_SameSectionDifferentName_ShouldBeAdded()
        {
            List<IRaceData> list = new List<IRaceData>();
            SectionTime st1 = new SectionTime("Henk", TimeSpan.FromSeconds(5), _section);
            st1.Add(list);
            SectionTime st2 = new SectionTime("Jasper", TimeSpan.FromSeconds(10), _section);
            st2.Add(list);

            Assert.Contains(st1, list);
            Assert.Contains(st2, list);
        }

        [Test]
        public void Add_SameNameDifferentSection_ShouldBeAdded()
        {
            List<IRaceData> list = new List<IRaceData>();
            SectionTime st1 = new SectionTime("Henk", TimeSpan.FromSeconds(5), _section);
            st1.Add(list);
            SectionTime st2 = new SectionTime("Henk", TimeSpan.FromSeconds(10), _section2);
            st2.Add(list);

            Assert.Contains(st1, list);
            Assert.Contains(st2, list);
        }

        [Test]
        public void Add_SameNameAndSection_ShouldntBeAdded()
        {
            List<IRaceData> list = new List<IRaceData>();
            SectionTime st1 = new SectionTime("Henk", TimeSpan.FromSeconds(5), _section);
            st1.Add(list);
            SectionTime st2 = new SectionTime("Henk", TimeSpan.FromSeconds(10), _section);
            st2.Add(list);

            Assert.Contains(st1, list);
            CollectionAssert.DoesNotContain(list, st2);
        }

        [Test]
        public void Add_SameNameAndSection_TimeIsReplaced()
        {
            TimeSpan newTime = TimeSpan.FromSeconds(10);

            List<IRaceData> list = new List<IRaceData>();
            SectionTime st1 = new SectionTime("Henk", TimeSpan.FromSeconds(5), _section);
            st1.Add(list);
            SectionTime st2 = new SectionTime("Henk", newTime, _section);
            st2.Add(list);

            Assert.AreEqual(newTime, st1.Time);
        }
    }
}
