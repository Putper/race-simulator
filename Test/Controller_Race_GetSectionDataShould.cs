﻿using Controller;
using Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Controller_Race_GetSectionDataShould
    {
        private List<IParticipant> _participants;
        private Track _track;

        [SetUp]
        public void Setup()
        {
            _participants = new List<IParticipant>() {
                new Driver("Spy", TeamColours.Red),
                new Driver("Soldier", TeamColours.Blue),
                new Driver("Olivia Mann", TeamColours.Green),
                new Driver("Saxton Hale", TeamColours.Yellow),
            };

            // a simple circle track
            _track = new Track("You Spin Me Right Round", new SectionTypes[] {
                SectionTypes.StartGrid,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.LeftCorner,
                SectionTypes.Finish
             });
        }

        /// <summary>
        /// when nothing has happened to the track
        /// you should still be able to get section data
        /// but it should contain empty data
        /// </summary>
        [Test]
        public void GetSectionData_EmptyTrack_IsEmptyData()
        {
            Race race = new Race(_track, _participants, 1);
            Section section = race.Track.Sections.First.Value;
            SectionData data = race.GetSectionData(section);

            Assert.IsNotNull(data);
            Assert.IsNull(data.Left);
            Assert.IsNull(data.Right);
            Assert.AreEqual(data.DistanceLeft, 0);
            Assert.AreEqual(data.DistanceRight, 0);
        }

        /// <summary>
        /// when participants are placed
        /// there should be participants on the track
        /// </summary>
        [Test]
        public void GetSectionData_PlacedParticipants_IsNotEmptyData()
        {
            Race race = new Race(_track, _participants, 1);
            race.PlaceParticipants();

            Section section = race.Track.Sections.First.Value;
            SectionData data = race.GetSectionData(section);

            Assert.IsNotNull(data);
            Assert.IsNotNull(data.Left);
            Assert.IsNotNull(data.Right);
        }
    }
}
