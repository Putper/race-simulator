﻿using Model.RaceData;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_ParticipantPoints_BestShould
    {
        [Test]
        public void Best_Empty_ShouldReturnNull()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantPoints pp1 = new ParticipantPoints("Henk", 1);

            string best = pp1.Best(list);
            Assert.AreEqual(null, best);
        }

        [Test]
        public void Best_ShouldReturnNameWithHighestPoints()
        {
            string winnerName = "Henk";

            List<IRaceData> list = new List<IRaceData>();
            ParticipantPoints pp1 = new ParticipantPoints("Nick", 1);
            pp1.Add(list);
            ParticipantPoints pp2 = new ParticipantPoints(winnerName, 10);
            pp2.Add(list);
            ParticipantPoints pp3 = new ParticipantPoints("Jasper", 5);
            pp3.Add(list);

            string best = pp1.Best(list);
            Assert.AreEqual(winnerName, best);
        }
    }
}
