﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Model;
using Controller;

namespace Test
{
    [TestFixture]
    class Controller_Race_RandomiseEquipmentShould
    {
        private Track _track;

        [SetUp]
        public void Setup()
        {
            // a simple circle track
            _track = new Track("You Spin Me Right Round", new SectionTypes[] {
                SectionTypes.StartGrid,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.LeftCorner,
                SectionTypes.Finish
             });
        }

        /// <summary>
        /// here we create a participants with unset values for it's car
        /// and ensure its set by the randomisation
        /// </summary>
        [Test]
        public void RandomiseEquipment_ShouldSet()
        {
            // create a single participant
            // with a car with no set values
            IEquipment car = new Car();
            List<IParticipant> participants = new List<IParticipant>() {
                new Driver("Spy", TeamColours.Red) { Equipment = car },
            };
            // create a race
            // only the participants are important for this test
            Race race = new Race(_track, participants, 1);

            race.RandomiseEquipment();
            // ensure is set by randomisation
            Assert.IsNotNull(car.Speed);
            Assert.IsNotNull(car.Performance);
        }
    }
}
