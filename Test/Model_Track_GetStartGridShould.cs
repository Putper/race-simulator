﻿using Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_Track_GetStartGridShould
    {

        [Test]
        public void GetStartGrid_CanFind()
        {
            Track track = new Track("name", new SectionTypes[] {
                SectionTypes.StartGrid,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.LeftCorner,
                SectionTypes.Finish
             });
            Assert.NotNull(track.GetStartGrid());
        }

        [Test]
        public void GetStartGrid_Empty_ReturnNull()
        {
            Track track = new Track("name", new SectionTypes[0]);
            Assert.Null(track.GetStartGrid());
        }
    }
}
