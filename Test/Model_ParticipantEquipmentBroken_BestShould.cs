﻿using Model.RaceData;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_ParticipantEquipmentBroken_BestShould
    {
        [Test]
        public void Best_Empty_ShouldReturnNull()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantEquipmentBroken p1 = new ParticipantEquipmentBroken("Henk", 1);

            string best = p1.Best(list);
            Assert.AreEqual(null, best);
        }

        [Test]
        public void Best_ShouldReturnNameWithLowestBroken()
        {
            string winnerName = "Henk";

            List<IRaceData> list = new List<IRaceData>();
            ParticipantEquipmentBroken p1 = new ParticipantEquipmentBroken("Nick", 10);
            p1.Add(list);
            ParticipantEquipmentBroken p2 = new ParticipantEquipmentBroken(winnerName, 1);
            p2.Add(list);
            ParticipantEquipmentBroken p3 = new ParticipantEquipmentBroken("Jasper", 5);
            p3.Add(list);

            string best = p1.Best(list);
            Assert.AreEqual(winnerName, best);
        }
    }
}
