﻿using Model.RaceData;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_ParticipantEquipmentBroken_AddShould
    {
        [Test]
        public void Add_DoesNotContain_ShouldBeAdded()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantEquipmentBroken p = new ParticipantEquipmentBroken("Henk", 5);
            p.Add(list);

            Assert.Contains(p, list);
        }

        [Test]
        public void Add_AlreadyContains_ShouldntBeAdded()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantEquipmentBroken p1 = new ParticipantEquipmentBroken("Henk", 5);
            p1.Add(list);
            ParticipantEquipmentBroken p2 = new ParticipantEquipmentBroken("Henk", 5);
            p2.Add(list);

            Assert.Contains(p1, list);
            CollectionAssert.DoesNotContain(list, p2);
        }

        [Test]
        public void Add_AlreadyContains_EquipmentBrokenShouldBeAdded()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantEquipmentBroken p1 = new ParticipantEquipmentBroken("Henk", 5);
            p1.Add(list);
            ParticipantEquipmentBroken p2 = new ParticipantEquipmentBroken("Henk", 5);
            p2.Add(list);

            Assert.AreEqual(p1.TimesBroken, 10);
        }
    }
}
