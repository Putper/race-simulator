﻿using Model.RaceData;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_ParticipantPoints_AddShould
    {
        [Test]
        public void Add_DoesNotContain_ShouldBeAdded()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantPoints pp = new ParticipantPoints("Henk", 5);
            pp.Add(list);

            Assert.Contains(pp, list);
        }

        [Test]
        public void Add_AlreadyContains_ShouldntBeAdded()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantPoints pp1 = new ParticipantPoints("Henk", 5);
            pp1.Add(list);
            ParticipantPoints pp2 = new ParticipantPoints("Henk", 5);
            pp2.Add(list);

            Assert.Contains(pp1, list);
            CollectionAssert.DoesNotContain(list, pp2);
        }

        [Test]
        public void Add_AlreadyContains_PointsShouldBeUpdated()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantPoints pp1 = new ParticipantPoints("Henk", 5);
            pp1.Add(list);
            ParticipantPoints pp2 = new ParticipantPoints("Henk", 5);
            pp2.Add(list);

            Assert.AreEqual(pp1.Points, 10);
        }
    }
}
