﻿using Model.RaceData;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_ParticipantTime_BestShould
    {
        [Test]
        public void Best_Empty_ShouldReturnNull()
        {
            List<IRaceData> list = new List<IRaceData>();
            ParticipantTime pt1 = new ParticipantTime("Henk", TimeSpan.FromSeconds(5));

            string best = pt1.Best(list);
            Assert.AreEqual(null, best);
        }

        [Test]
        public void Best_ShouldReturnNameWithLowestTime()
        {
            string winnerName = "Henk";

            List<IRaceData> list = new List<IRaceData>();
            ParticipantTime pt1 = new ParticipantTime("Nick", TimeSpan.FromSeconds(10));
            pt1.Add(list);
            ParticipantTime pt2 = new ParticipantTime(winnerName, TimeSpan.FromSeconds(1));
            pt2.Add(list);
            ParticipantTime pt3 = new ParticipantTime("Jasper", TimeSpan.FromSeconds(5));
            pt3.Add(list);

            string best = pt1.Best(list);
            Assert.AreEqual(winnerName, best);
        }
    }
}
