﻿using NUnit.Framework;
using Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_Compass_DirectionShouldBe
    {
        private Compass _compass { get; set; }

        [SetUp]
        public void SetUp()
        {
            _compass = new Compass(Directions.North);
        }

        [Test]
        public void IsHorizontal_East_ReturnTrue()
        {
            _compass.Direction = Directions.East;
            Assert.IsTrue(_compass.IsHorizontal());
        }

        [Test]
        public void IsHorizontal_West_ReturnTrue()
        {
            _compass.Direction = Directions.West;
            Assert.IsTrue(_compass.IsHorizontal());
        }

        [Test]
        public void Equals_ReturnsCorrect()
        {
            Assert.IsTrue(_compass.Equals(Directions.North));
            Assert.IsFalse(_compass.Equals(Directions.West));
        }

        [Test]
        public void RotateClockWise_ReturnDirection()
        {
            _compass.RotateClockwise();
            Assert.AreEqual(_compass.Direction, Directions.East);
            _compass.RotateClockwise();
            Assert.AreEqual(_compass.Direction, Directions.South);
            _compass.RotateClockwise();
            Assert.AreEqual(_compass.Direction, Directions.West);
            _compass.RotateClockwise();
            Assert.AreEqual(_compass.Direction, Directions.North);
        }

        [Test]
        public void RotateAntiClockWise_ReturnDirection()
        {
            _compass.RotateAntiClockwise();
            Assert.AreEqual(_compass.Direction, Directions.West);
            _compass.RotateAntiClockwise();
            Assert.AreEqual(_compass.Direction, Directions.South);
            _compass.RotateAntiClockwise();
            Assert.AreEqual(_compass.Direction, Directions.East);
            _compass.RotateAntiClockwise();
            Assert.AreEqual(_compass.Direction, Directions.North);
        }
    }
}
