﻿using Model;
using Model.RaceData;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_SectionTime_BestShould
    {
        private Section _section { get; set; }
        private Section _section2 { get; set; }
        private Section _section3 { get; set; }

        [SetUp]
        public void SetUp()
        {
            _section = new Section() { SectionType = SectionTypes.StartGrid };
            _section2 = new Section() { SectionType = SectionTypes.Straight };
            _section3 = new Section() { SectionType = SectionTypes.Finish };
        }

        [Test]
        public void Best_Empty_ShouldReturnNull()
        {
            List<IRaceData> list = new List<IRaceData>();
            SectionTime st1 = new SectionTime("Henk", TimeSpan.FromSeconds(5), _section);

            string best = st1.Best(list);
            Assert.AreEqual(null, best);
        }

        [Test]
        public void Best_ShouldReturnNameWithLowestTime()
        {
            string winnerName = "Henk";

            List<IRaceData> list = new List<IRaceData>();
            SectionTime st1 = new SectionTime("Nick", TimeSpan.FromSeconds(10), _section);
            st1.Add(list);
            SectionTime st2 = new SectionTime(winnerName, TimeSpan.FromSeconds(1), _section2);
            st2.Add(list);
            SectionTime st3 = new SectionTime("Jasper", TimeSpan.FromSeconds(5), _section3);
            st3.Add(list);

            string best = st1.Best(list);
            Assert.AreEqual(winnerName, best);
        }
    }
}
