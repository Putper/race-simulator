﻿using Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    [TestFixture]
    class Model_TrackDimensions_FromTrackShould
    {
        /// <summary>
        /// Create a track that only goes to the bottom and the right.
        /// Meaning it shouldnt have to have a different starting position.
        /// </summary>
        [Test]
        public void FromTrack_ToBottomRight_ShouldBe0()
        {
            Track track = new Track("name", new SectionTypes[] {
                SectionTypes.StartGrid,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Finish,
             });

            TrackDimensions dimensions = TrackDimensions.FromTrack(track, Directions.East);
            Assert.AreEqual(0, dimensions.StartXPos);
            Assert.AreEqual(0, dimensions.StartYPos);
        }

        /// <summary>
        /// Create a track that is 4x4 and check if dimensions are correct
        /// This track goes to the bottom right
        /// </summary>
        [Test]
        public void FromTrack_ToBottomRight_WidthShouldBe4()
        {
            Track track = new Track("name", new SectionTypes[] {
                SectionTypes.StartGrid,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Finish,
             });

            TrackDimensions dimensions = TrackDimensions.FromTrack(track, Directions.East);
            Assert.AreEqual(4, dimensions.Width);
            Assert.AreEqual(4, dimensions.Height);
        }

        /// <summary>
        /// Create a track that only goes to the top left
        /// Meaning it should alter the starting position to compensate
        /// </summary>
        [Test]
        public void FromTrack_ToTopLeft_ShouldBe4()
        {
            Track track = new Track("name", new SectionTypes[] {
                SectionTypes.StartGrid,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.RightCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Finish,
             });

            TrackDimensions dimensions = TrackDimensions.FromTrack(track, Directions.West);
            Assert.AreEqual(3, dimensions.StartXPos);
            Assert.AreEqual(3, dimensions.StartYPos);
        }

        /// <summary>
        /// Create a track that is 4x4 and check if dimensions are correct.
        /// This track goes to the top left
        /// </summary>
        [Test]
        public void FromTrack_ToTopLeft_WidthShouldBe4()
        {
            Track track = new Track("name", new SectionTypes[] {
                SectionTypes.StartGrid,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.LeftCorner,
                SectionTypes.Straight,
                SectionTypes.Straight,
                SectionTypes.Finish,
             });

            TrackDimensions dimensions = TrackDimensions.FromTrack(track, Directions.West);
            Assert.AreEqual(4, dimensions.Width);
            Assert.AreEqual(4, dimensions.Height);
        }
    }
}
