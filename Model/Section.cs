﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public enum SectionTypes
    {
        Straight,
        LeftCorner,
        RightCorner,
        StartGrid,
        Finish
    }

    public enum CornerTypes
    {
        RightToTop,
        RightToBottom,
        LeftToTop,
        LeftToBottom,
    }

    public class Section
    {
        public SectionTypes SectionType { get; set; }

        public static CornerTypes GetCornerType(SectionTypes sectionType, Directions direction)
        {
            if(sectionType == SectionTypes.RightCorner)
            {
                switch (direction)
                {
                    case Directions.North:
                        return CornerTypes.RightToBottom;
                    case Directions.East:
                        return CornerTypes.LeftToBottom;
                    case Directions.South:
                        return CornerTypes.LeftToTop;
                    default: // Directions.West
                        return CornerTypes.RightToTop;
                }
            }
            else // left corner
            {

                switch (direction)
                {
                    case Directions.North:
                        return CornerTypes.LeftToBottom;
                    case Directions.East:
                        return CornerTypes.LeftToTop;
                    case Directions.South:
                        return CornerTypes.RightToTop;
                    default: // Directions.West
                        return CornerTypes.RightToBottom;
                }
            }
        }
    }
}
