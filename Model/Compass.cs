﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public enum Directions
    {
        North,
        East,
        South,
        West
    }
    public class Compass
    {
        public Directions Direction { get; set; }

        public Compass(Directions direction)
        {
            Direction = direction;
        }

        public void RotateClockwise()
        {
            switch(Direction)
            {
                case Directions.North:
                    Direction = Directions.East;
                    break;
                case Directions.East:
                    Direction = Directions.South;
                    break;
                case Directions.South:
                    Direction = Directions.West;
                    break;
                case Directions.West:
                    Direction = Directions.North;
                    break;
            }
        }

        public void RotateAntiClockwise()
        {
            switch (Direction)
            {
                case Directions.North:
                    Direction = Directions.West;
                    break;
                case Directions.East:
                    Direction = Directions.North;
                    break;
                case Directions.South:
                    Direction = Directions.East;
                    break;
                case Directions.West:
                    Direction = Directions.South;
                    break;
            }
        }

        public bool IsHorizontal()
        {
            return Direction == Directions.West || Direction == Directions.East;
        }

        /// <summary>
        /// direction to an integer
        /// </summary>
        /// <returns>north=0, east=1, south=2, west=3</returns>
        public int ToInt()
        {
            switch (Direction)
            {
                case Directions.East:
                    return 1;
                case Directions.South:
                    return 2;
                case Directions.West:
                    return 3;
                default:
                    return 0;
            }
        }

        public bool Equals(Directions direction)
        {
            return Direction == direction;
        }
    }
}
