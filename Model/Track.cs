﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Track
    {
        public string Name { get; set; }
        public LinkedList<Section> Sections { get; set; }

        public Track(string name, SectionTypes[] sections)
        {
            Name = name;
            Sections = CreateSectionsLinkedList(sections);
        }

        /// <summary>
        /// Get the start grid of this track
        /// </summary>
        /// <returns>the start grid section, null if there isnt any</returns>
        public LinkedListNode<Section> GetStartGrid()
        {
            LinkedListNode<Section> section = Sections.First;
            while(section != null)
            {
                if(section.Value.SectionType == SectionTypes.StartGrid)
                {
                    return section;
                }
                section = section.Next;
            }
            return null;
        }

        // Convert an array of SectionTypes to a LinkedList of Sections
        private LinkedList<Section> CreateSectionsLinkedList(SectionTypes[] sectionTypes)
        {
            LinkedList<Section> sections = new LinkedList<Section>();
            foreach (SectionTypes sectionType in sectionTypes)
            {
                Section section = new Section();
                section.SectionType = sectionType;
                sections.AddLast(section);
            }
            return sections;
        }
    }
}
