﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Driver : IParticipant
    {
        public string Name { get; set; }
        public int Points { get; set; }
        public IEquipment Equipment { get; set; }
        public TeamColours TeamColor { get; set; }

        public Driver(string name, TeamColours teamColor)
        {
            Name = name;
            TeamColor = teamColor;
            Equipment = new Car();
            Points = 0;
        }
    }
}
