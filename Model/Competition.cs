﻿using Model.RaceData;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Competition
    {
        /// <summary>
        /// List of participants participating in this competition
        /// </summary>
        public List<IParticipant> Participants { get; set; }
        /// <summary>
        /// Queue of all the tracks which will be driven on during the competition
        /// </summary>
        public Queue<Track> Tracks { get; set; }

        /// <summary>
        /// List of each participant and their points
        /// </summary>
        public RaceDataList<ParticipantPoints> ParticipantsPoints = new RaceDataList<ParticipantPoints>();
        public RaceDataList<ParticipantEquipmentBroken> ParticipantsEquipmentBroken = new RaceDataList<ParticipantEquipmentBroken>();
        public RaceDataList<ParticipantFailedToFixEquipment> ParticipantsFailedToFixEquipment = new RaceDataList<ParticipantFailedToFixEquipment>();

        public Competition()
        {
            Participants = new List<IParticipant>();
            Tracks = new Queue<Track>();
        }

        /// <summary>
        /// get the next track
        /// </summary>
        /// <returns>the next track, null if there are no tracks left</returns>
        public Track NextTrack()
        {
            if(Tracks.Count == 0)
            {
                return null;
            }
            return Tracks.Dequeue();
        }

        /// <summary>
        /// add points for each participants based on position
        /// </summary>
        /// <param name="participants">participants enqueued in order of finishing their race</param>
        public void AddParticipantPoints(Queue<IParticipant> participants)
        {
            int points = participants.Count;
            while (participants.Count != 0)
            {
                IParticipant participant = participants.Dequeue();
                ParticipantsPoints.Add(new ParticipantPoints(participant.Name, points));
                points -= 1;
            }
        }

        /// <summary>
        /// register how many times each participant had their equipment break
        /// </summary>
        /// <param name="participants">dictionary where key is participant and value the amount of times their equipment broke</param>
        public void AddParticipantsEquipmentBroken(Dictionary<IParticipant, int> participants)
        {
            foreach (KeyValuePair<IParticipant, int> entry in participants)
            {
                ParticipantsEquipmentBroken.Add(new ParticipantEquipmentBroken(entry.Key.Name, entry.Value));
            }
        }

        /// <summary>
        /// register how many times each participant failed to fix their equipment
        /// </summary>
        /// <param name="participants">dictionary where key is participant and value the amount of times they failed to fix the equipment</param>
        public void AddParticipantFailedToFixEquipment(Dictionary<IParticipant, int> participants)
        {
            foreach (KeyValuePair<IParticipant, int> entry in participants)
            {
                ParticipantsFailedToFixEquipment.Add(new ParticipantFailedToFixEquipment(entry.Key.Name, entry.Value));
            }
        }
    }
}
