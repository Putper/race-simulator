﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class TrackDimensions
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int StartXPos { get; set; }
        public int StartYPos { get; set; }
        
        public TrackDimensions(int width, int height, int startXPos, int startYPos)
        {
            Width = width;
            Height = height;
            StartXPos = startXPos;
            StartYPos = startYPos;
        }


        public static TrackDimensions FromTrack(Track track, Directions startDirection, int sectionSize = 1)
        {
            Compass compass = new Compass(startDirection);
            int xPos = 0;
            int yPos = 0;
            int lowestXPos = 0;
            int lowestYPos = 0;
            int highestXPos = 0;
            int highestYPos = 0;

            // calculating initial x&y pos
            bool first = true;
            foreach (Section section in track.Sections)
            {
                // don't move forward for the first one cuz its always drawn on 0,0
                if (!first)
                {
                    // move one forward
                    switch (compass.Direction)
                    {
                        case Directions.North:
                            yPos -= 1;
                            break;
                        case Directions.East:
                            xPos += 1;
                            break;
                        case Directions.South:
                            yPos += 1;
                            break;
                        default: // Directions.West
                            xPos -= 1;
                            break;
                    }
                }
                first = false;

                // handle new direction for corners
                if (section.SectionType == SectionTypes.RightCorner)
                {
                    compass.RotateClockwise();
                }
                else if (section.SectionType == SectionTypes.LeftCorner)
                {
                    compass.RotateAntiClockwise();
                }

                // store if lower
                if (xPos < lowestXPos)
                {
                    lowestXPos = xPos;
                }
                if (yPos < lowestYPos)
                {
                    lowestYPos = yPos;
                }
                // store if higher
                if (xPos > highestXPos)
                {
                    highestXPos = xPos;
                }
                if (yPos > highestYPos)
                {
                    highestYPos = yPos;
                }
            }


            int width = (highestXPos - lowestXPos)+1;
            int height = (highestYPos - lowestYPos)+1;

            // change to positive number
            if (lowestXPos < 0)
            {
                lowestXPos = lowestXPos * -1;
            }
            if (lowestYPos < 0)
            {
                lowestYPos = lowestYPos * -1;
            }

            return new TrackDimensions(width * sectionSize, height * sectionSize, lowestXPos*sectionSize, lowestYPos * sectionSize);
        }
    }
}
