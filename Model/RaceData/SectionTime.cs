﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.RaceData
{
    public class SectionTime : ParticipantTime, IRaceData
    {
        public Section Section { get; set; }

        public SectionTime(string name, TimeSpan time, Section section) : base(name, time)
        {
            Section = section;
        }

        /// <summary>
        /// adds this item to the given list
        /// or replaces time if it already exists
        /// </summary>
        /// <param name="list"></param>
        public new void Add(List<IRaceData> list)
        {
            // its the same if participant and section are teh same
            IEnumerable<IRaceData> existingParticipant = list.Where(item =>
            {
                SectionTime sectionTime = item as SectionTime;
                return sectionTime != null && sectionTime.Name == Name && sectionTime.Section == Section;
            });

            if (existingParticipant.Any())
            {
                SectionTime existing = (SectionTime)existingParticipant.First();
                existing.Time = Time;
            }
            else
            {
                list.Add(this);
            }
        }
    }
}
