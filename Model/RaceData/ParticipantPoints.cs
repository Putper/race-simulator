﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.RaceData
{
    public class ParticipantPoints : IRaceData
    {
        public string Name { get; set; }
        public int Points { get; set; }

        public ParticipantPoints(string name, int points)
        {
            Name = name;
            Points = points;
        }

        /// <summary>
        /// adds this item to the given list
        /// </summary>
        /// <param name="list"></param>
        public void Add(List<IRaceData> list)
        {
            IEnumerable<IRaceData> existingParticipant = list.Where(item => item.Name == Name);
            if (existingParticipant.Any())
            {
                ParticipantPoints existing = (ParticipantPoints)existingParticipant.First();
                existing.Points += Points;
            }
            else
            {
                list.Add(this);
            }
        }


        public string Best(List<IRaceData> list)
        {
            if (!list.Any())
            {
                return null;
            }

            ParticipantPoints highest = list[0] as ParticipantPoints;
            for (int i = 1; i < list.Count; ++i)
            {
                ParticipantPoints castedItem = list[i] as ParticipantPoints;
                if (highest == null || castedItem != null && castedItem.Points > highest.Points)
                {
                    highest = castedItem;
                }
            }

            return highest?.Name;
        }
    }
}
