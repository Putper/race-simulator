﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.RaceData
{
    public class ParticipantTime : IRaceData
    {
        public string Name { get; set; }
        public TimeSpan Time { get; set; }

        public ParticipantTime(string name, TimeSpan time)
        {
            Name = name;
            Time = time;
        }

        /// <summary>
        /// adds this item to the given list
        /// or replaces time if it already exists
        /// </summary>
        /// <param name="list"></param>
        public void Add(List<IRaceData> list)
        {
            // its the same if participant and section are teh same
            IEnumerable<IRaceData> existingParticipant = list.Where(item => item.Name == Name);

            if (existingParticipant.Any())
            {
                ParticipantTime existing = (ParticipantTime)existingParticipant.First();
                existing.Time = Time;
            }
            else
            {
                list.Add(this);
            }
        }

        /// <summary>
        /// return participant with lowest time
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public string Best(List<IRaceData> list)
        {
            if (!list.Any())
            {
                return null;
            }

            ParticipantTime lowest = list[0] as ParticipantTime;
            for (int i = 1; i < list.Count; ++i)
            {
                ParticipantTime castedItem = list[i] as ParticipantTime;
                if (lowest == null || castedItem != null && TimeSpan.Compare(castedItem.Time, lowest.Time) < 0)
                {
                    lowest = castedItem;
                }
            }

            return lowest?.Name;
        }
    }
}
