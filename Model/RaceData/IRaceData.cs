﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.RaceData
{
    public interface IRaceData
    {
        public string Name { get; set; }

        /// <summary>
        /// add this object to given list
        /// </summary>
        /// <param name="list"></param>
        public void Add(List<IRaceData> list);

        /// <summary>
        /// get best out of list
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public string Best(List<IRaceData> list);
    }
}
