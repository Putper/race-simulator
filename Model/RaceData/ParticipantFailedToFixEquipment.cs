﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.RaceData
{
    public class ParticipantFailedToFixEquipment : IRaceData
    {
        public string Name { get; set; }

        public int TimesFailedToFix { get; set; }

        public ParticipantFailedToFixEquipment(string name, int timesFailedToFix)
        {
            Name = name;
            TimesFailedToFix = timesFailedToFix;
        }

        /// <summary>
        /// adds this item to the given list
        /// </summary>
        /// <param name="list"></param>
        public void Add(List<IRaceData> list)
        {
            IEnumerable<IRaceData> existingParticipant = list.Where(item => item.Name == Name);
            if (existingParticipant.Any())
            {
                ParticipantFailedToFixEquipment existing = (ParticipantFailedToFixEquipment)existingParticipant.First();
                existing.TimesFailedToFix += TimesFailedToFix;
            }
            else
            {
                list.Add(this);
            }
        }


        public string Best(List<IRaceData> list)
        {
            if (!list.Any())
            {
                return null;
            }

            ParticipantFailedToFixEquipment lowest = list[0] as ParticipantFailedToFixEquipment;
            for (int i = 1; i < list.Count; ++i)
            {
                ParticipantFailedToFixEquipment castedItem = list[i] as ParticipantFailedToFixEquipment;
                if (lowest == null || castedItem != null && castedItem.TimesFailedToFix < lowest.TimesFailedToFix)
                {
                    lowest = castedItem;
                }
            }

            return lowest?.Name;
        }
    }
}
