﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.RaceData
{
    public class ParticipantEquipmentBroken : IRaceData
    {
        public string Name { get; set; }

        public int TimesBroken { get; set; }

        public ParticipantEquipmentBroken(string name, int timesBroken)
        {
            Name = name;
            TimesBroken = timesBroken;
        }

        /// <summary>
        /// adds this item to the given list
        /// </summary>
        /// <param name="list"></param>
        public void Add(List<IRaceData> list)
        {
            IEnumerable<IRaceData> existingParticipant = list.Where(item => item.Name == Name);
            if (existingParticipant.Any())
            {
                ParticipantEquipmentBroken existing = (ParticipantEquipmentBroken)existingParticipant.First();
                existing.TimesBroken += TimesBroken;
            }
            else
            {
                list.Add(this);
            }
        }

        public string Best(List<IRaceData> list)
        {
            if(!list.Any())
            {
                return null;
            }

            ParticipantEquipmentBroken lowest = list[0] as ParticipantEquipmentBroken;
            for(int i=1; i<list.Count; ++i)
            {
                ParticipantEquipmentBroken castedItem = list[i] as ParticipantEquipmentBroken;
                if (lowest == null || castedItem != null && castedItem.TimesBroken < lowest.TimesBroken)
                {
                    lowest = castedItem;
                }
            }

            return lowest?.Name;
        }
    }
}
