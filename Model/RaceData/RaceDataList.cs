﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.RaceData
{
    public class RaceDataList<T> where T : IRaceData
    {
        private List<IRaceData> _list = new List<IRaceData>();

        public void Add(IRaceData item)
        {
            item.Add(_list);
        }

        public string Best()
        {
            if(!_list.Any())
            {
                return null;
            }

            return _list[0].Best(_list);
        }

        /// <summary>
        /// get list
        /// </summary>
        /// <returns>list</returns>
        public List<IRaceData> GetList()
        {
            return _list;
        }
    }
}   
